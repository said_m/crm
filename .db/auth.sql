DROP TABLE IF EXISTS `auth_users`;
CREATE TABLE `auth_users` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `sys_userID` int(10) unsigned,
  `login` varchar(50) NOT NULL default '',
  `password` varchar(50) NOT NULL default '',
  `sys_name` varchar(50) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `auth_login_attempts`;
CREATE TABLE `auth_login_attempts` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `remoteIP` varchar(15) NOT NULL default '',
  `proxyIP` varchar(15) NOT NULL default '',
  `quantity` int(10) unsigned default NULL,
  `mtime` datetime default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `auth_sessions`;
CREATE TABLE `auth_sessions` (
  `mtime` datetime default NULL,
  `userID` int(10) unsigned default NULL,
  `sessID` varchar(40) default NULL,
  `remoteIP` varchar(15) NOT NULL default 'none',
  `proxyIP` varchar(15) NOT NULL default 'none',
  KEY `userID` (`userID`),
  CONSTRAINT `auth_sessions_ibfk_1` FOREIGN KEY (`userID`) REFERENCES `auth_users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `auth_users` (`id`, `sys_userID`, `login`, `password`, `sys_name`) VALUES (1, 1, 'ddo', '111', 'my_erp');