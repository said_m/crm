SET FOREIGN_KEY_CHECKS=0;
SET CHARACTER SET UTF8;

INSERT INTO `erp_modules` (`id`, `title`, `uri`, `mtype`, `sort`, `mobile`) VALUES 
  (1,'Пользователи','/musers/index.html','user',1,'0'),
  (2,'CRM','/mcrm/index.html','user',1,'1'),
  (3,'Структура','/mstructure/index.html','admin',2,'0'),
  (4,'Товары','/minventories/index.html','user',2,'1'),
  (5,'Заказы','/morders/index.html','user',3,'1'),
  (6,'Справочники','/mcategories/index.html','admin',3,'0');

INSERT INTO `erp_users` (`id`, `user_ogin`, `user_word`, `user_fname`, `user_lname`) VALUES 
  (1,'root','11111','Admin','');

INSERT INTO `erp_user2module` (`moduleID`, `userID`, `perm`) VALUES 
  (1,1,3),
  (2,1,3),
  (3,1,3),
  (4,1,3),
  (5,1,3),
  (6,1,3);

INSERT INTO `erp_task_categories` (`id`, `parent`, `lft`, `rgt`, `level`, `title`, `sdesc`, `fdesc`) VALUES 
  (1,0,1,2,1,'Root node',NULL,NULL);

INSERT INTO `erp_crm_groups` (`id`, `parent`, `lft`, `rgt`, `level`, `title`, `sdesc`, `fdesc`) VALUES 
  (1,0,1,2,1,'Root node',NULL,NULL);



SET FOREIGN_KEY_CHECKS=1;
