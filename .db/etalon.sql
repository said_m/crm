SET FOREIGN_KEY_CHECKS=0;

DROP TABLE IF EXISTS `erp_modules`;
CREATE TABLE `erp_modules` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `title` varchar(45) NOT NULL default '',
  `uri` varchar(50) default NULL,
  `mtype` enum('admin','user') default 'user',
  `sort` tinyint(3) unsigned default '1',
  `mobile` enum('0','1') default '0',
  `style_id` int(3) default 1,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `erp_users`;
CREATE TABLE `erp_users` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `user_ogin` varchar(50) NOT NULL default '',
  `user_word` varchar(50) NOT NULL default '',
  `user_fname` varchar(100) default NULL,
  `user_lname` varchar(100) default NULL,
  `user_mname` varchar(100) default NULL,
  `user_address1` varchar(100) default NULL,
  `user_address2` varchar(100) default NULL,
  `country` varchar(50) default 'Belarus',
  `user_city` varchar(50) default NULL,
  `user_state` varchar(50) default NULL,
  `user_postcode` varchar(10) default NULL,
  `user_phone1` varchar(30) default NULL,
  `user_phone2` varchar(30) default NULL,
  `user_emerg_contperson` varchar(100) default NULL,
  `user_phone_emerg` varchar(30) default NULL,
  `user_fax` varchar(30) default NULL,
  `user_mobile` varchar(30) default NULL,
  `user_birthdate` date default NULL,
  `user_position` varchar(100) default NULL,
  `user_payunit` enum('h','d','w','m') default 'm',
  `comments` text,
  `user_notes` text,
  `user_email` varchar(100) default NULL,
  `user_comp` int(10) unsigned default NULL,
  `created` datetime default NULL,
  `deleted` enum('0','1') default '0',
  `user_status` enum('1','2','3','4','5','6','7','8','9') default '1',
  `sort` int(10) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `erp_user2module`;
CREATE TABLE `erp_user2module` (
  `moduleID` int(10) unsigned NOT NULL default '0',
  `userID` int(10) unsigned NOT NULL default '0',
  `perm` tinyint(3) unsigned NOT NULL default '0',
  PRIMARY KEY  (`moduleID`,`userID`),
  KEY `userID` (`userID`),
  CONSTRAINT `erp_user2module_ibfk_1` FOREIGN KEY (`moduleID`) REFERENCES `erp_modules` (`id`) ON DELETE CASCADE,
  CONSTRAINT `erp_user2module_ibfk_2` FOREIGN KEY (`userID`) REFERENCES `erp_users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `erp_crm_messages`;
CREATE TABLE `erp_crm_messages` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `userID` int(10) unsigned NOT NULL default '0',
  `touserID` int(10) unsigned default NULL,
  `created` datetime default NULL,
  `message` text,
  `deleted` enum('0','1') default '0',
  `viewed` enum('0','1') default '0',
  `basic` enum('1','2') default '1',
  `cc_users` text,
  `smsRecipients` text,
  PRIMARY KEY  (`id`),
  KEY `userID` (`userID`),
  CONSTRAINT `erp_crm_messages_ibfk_1` FOREIGN KEY (`userID`) REFERENCES `erp_users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `erp_task_categories`;
CREATE TABLE `erp_task_categories` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `parent` int(10) unsigned default '0',
  `lft` int(10) unsigned NOT NULL default '0',
  `rgt` int(10) unsigned NOT NULL default '0',
  `level` int(10) unsigned NOT NULL default '0',
  `title` varchar(200) NOT NULL default '',
  `sdesc` text,
  `fdesc` text,
  `isHidden` enum('0','1') default '0',
   PRIMARY KEY  (`id`),
  KEY `parent_idx` (`parent`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `erp_crm_groups`;
CREATE TABLE `erp_crm_groups` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `parent` int(10) unsigned default '0',
  `lft` int(10) unsigned NOT NULL default '0',
  `rgt` int(10) unsigned NOT NULL default '0',
  `level` int(10) unsigned NOT NULL default '0',
  `title` varchar(200) NOT NULL default '',
  `sdesc` text,
  `fdesc` text,
  `isHidden` enum('0','1') default '0',
   PRIMARY KEY  (`id`),
  KEY `parent_idx` (`parent`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `erp_crm_tasks`;
CREATE TABLE `erp_crm_tasks` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `userID` int(10) unsigned NOT NULL default '0',
  `contactID` int(10) unsigned default NULL,
  `categoryID` int(10) unsigned default NULL,
  `state` enum('open','done','not done','attempted','received call','left message','held','not held','postponed','deleted') default 'open',
  `priority` enum('high','medium','low') default 'medium',
  `created` datetime default NULL,
  `expiry_date` date default NULL,
  `expiry_time` time default NULL,
  `title` varchar(150) NOT NULL default '',
  `about` text,
  `comments` text,
  `deleted` enum('0','1') default '0',
  `createbyID` int(10) unsigned default NULL,
  PRIMARY KEY  (`id`),
  KEY `userID` (`userID`),
  KEY `contactID` (`contactID`),
  KEY `createbyID` (`createbyID`),
  KEY `categoryID` (`categoryID`),
  CONSTRAINT `erp_crm_tasks_ibfk_1` FOREIGN KEY (`userID`) REFERENCES `erp_users` (`id`),
  CONSTRAINT `erp_crm_tasks_ibfk_2` FOREIGN KEY (`contactID`) REFERENCES `erp_crm_contacts` (`id`),
  CONSTRAINT `erp_crm_tasks_ibfk_3` FOREIGN KEY (`createbyID`) REFERENCES `erp_users` (`id`),
  CONSTRAINT `erp_crm_tasks_ibfk_4` FOREIGN KEY (`categoryID`) REFERENCES `erp_task_categories` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `erp_crm_contacts`;
CREATE TABLE `erp_crm_contacts` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `cont_title` varchar(200) default '',
  `created` datetime default NULL,
  `crm_personID` int(10) unsigned default NULL,
  `groupID` int(10) unsigned default NULL,
  `userID` int(10) unsigned default NULL,
  `deleted` enum('0','1') default '0',
  `last_used` datetime default NULL,
  `comp_name` varchar(100) NOT NULL default '',
  `comp_contfn` varchar(30) NOT NULL default '',
  `comp_contln` varchar(30) NOT NULL default '',
  `comp_contnick` varchar(30) NOT NULL default '',
  `comp_conttitle` varchar(30) NOT NULL default '',
  `comp_phone` varchar(30) NOT NULL default '',
  `comp_fax` varchar(30) NOT NULL default '',
  `comp_mobile` varchar(30) NOT NULL default '',
  `comp_discount` decimal(5,2) default '0.00',
  `comp_address1` varchar(100) NOT NULL default '',
  `comp_address2` varchar(100) NOT NULL default '',
  `comp_city` varchar(40) NOT NULL default '',
  `comp_postcode` varchar(20) NOT NULL default '',
  `comp_country` varchar(30) NOT NULL default 'United States',
  `comp_state` varchar(20) NOT NULL default '',
  `comp_site` varchar(30) NOT NULL default '',
  `comp_email` varchar(100) NOT NULL default '',
  `about` text,
  PRIMARY KEY  (`id`),
  KEY `crm_personID` (`crm_personID`),
  KEY `groupID` (`groupID`),
  KEY `userID` (`userID`),
  CONSTRAINT `erp_crm_contacts_ibfk_1` FOREIGN KEY (`crm_personID`) REFERENCES `erp_crm_persons` (`id`),
  CONSTRAINT `erp_crm_contacts_ibfk_2` FOREIGN KEY (`groupID`) REFERENCES `erp_crm_groups` (`id`),
  CONSTRAINT `erp_crm_contacts_ibfk_3` FOREIGN KEY (`userID`) REFERENCES `erp_users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `erp_crm_persons`;
CREATE TABLE `erp_crm_persons` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `user_fname` varchar(30) NOT NULL default '',
  `user_lname` varchar(30) NOT NULL default '',
  `user_mname` varchar(30) NOT NULL default '',
  `user_birthdate` date default NULL,
  `user_email` varchar(100) NOT NULL default '',
  `country1` varchar(90) NOT NULL default 'United States',
  `user_state1` varchar(20) NOT NULL default '',
  `user_city1` varchar(120) NOT NULL default '',
  `user_postcode1` varchar(60) NOT NULL default '',
  `user_address1` text NOT NULL,
  `user_address1_l2` varchar(100) NOT NULL default '',
  `country2` varchar(90) NOT NULL default 'United States',
  `user_state2` varchar(20) NOT NULL default '',
  `user_city2` text NOT NULL,
  `user_postcode2` varchar(60) NOT NULL default '',
  `user_address2` text NOT NULL,
  `user_address2_l2` varchar(100) NOT NULL default '',
  `user_phone` varchar(30) NOT NULL default '',
  `user_mobile` varchar(30) NOT NULL default '',
  `user_fax` varchar(30) NOT NULL default '',
  `user_hphone` varchar(30) NOT NULL default '',
  `user_altphone1` varchar(30) NOT NULL default '',
  `user_altphone2` varchar(30) NOT NULL default '',
  `user_extra_email` varchar(100) NOT NULL default '',
  `user_position` varchar(100) default NULL,
  `user_extra_email2` varchar(100) default NULL,
  `user_extra_email3` varchar(100) default NULL,
  `user_sms` varchar(30) NOT NULL default '',
  `about` text,
  `userID` int(10) unsigned default NULL,
  `created` datetime default NULL,
  `deleted` enum('0','1') default '0',
  `last_used` datetime default NULL,
  `contactID` int(10) unsigned default NULL,
  PRIMARY KEY  (`id`),
  KEY `userID` (`userID`),
  KEY `contactID` (`contactID`),
  CONSTRAINT `erp_crm_persons_ibfk_1` FOREIGN KEY (`userID`) REFERENCES `erp_users` (`id`),
  CONSTRAINT `erp_crm_persons_ibfk_2` FOREIGN KEY (`contactID`) REFERENCES `erp_crm_contacts` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


SET FOREIGN_KEY_CHECKS=1;
