<?php

define("PREFIX", dirname($_SERVER['SCRIPT_FILENAME']) . '/');
include_once(PREFIX . '.engine/lib/bootstrap.php');

if (isset($_SESSION['sys_name']) && strlen($_SESSION['sys_name'])) 
	define("SYSNAME", $_SESSION['sys_name']);
else define("SYSNAME", '');
//Logger::getInstance()->debug($_POST);
//Logger::getInstance()->debug($_FILES);
//print_r(Config::getInstance());

if (isset($_POST) && count($_POST) > 0 && isset($_POST['action'])) {
    header("Cache-Control: no-store, no-cache, must-revalidate");
    header("Cache-Control: post-check=0, pre-check=0", false);
    header("Pragma: no-cache");
    $responseJson = array();
    $responseJson['success'] = 0;
    $responseJson['messages'] = array();
    $responseJson['errors'] = array();
    


    if (in_array($_POST['action'], array('login', 'logout'))) {
        $responseJson['action'] = $_POST['action'];
        switch ($_POST['action'])
        {
            case 'login':
                if (isset($Args['AuthUser'])) unset($Args['AuthUser']);
                if (isset($_SESSION["AuthUser"])) unset($_SESSION["AuthUser"]);
                
                $Args['post'] = Misc::post($_POST['enter']);
                $Captcha = new Captcha(array('fonts_dir' => PREFIX . '.engine/lib/fonts/'));
                
				if (isset($_POST['code']) && $Captcha->check($_POST['code']) != false) {
                    if (!isset($Args['post']['ogin']) || !strlen($Args['post']['ogin'])) $responseJson['errors']['form']['ogin'] = _('Поле Логин заполнено некорректно');
                    if (!isset($Args['post']['word']) || !strlen($Args['post']['word'])) $responseJson['errors']['form']['word'] = _('Поле Пароль заполнено некорректно');
                    if (!isset($responseJson['errors']) || count($responseJson['errors']) == 0) {
						try {
							$Args['AuthUser'] = AuthUser::authorize($Args['post']['ogin'], md5($Args['post']['word']));
							//Logger::getInstance()->debug(print_r($Args['AuthUser'], true));
							
							if (is_object($Args['AuthUser']) && $Args['AuthUser']->id > 0) {
                                //if (isset($_POST['_rem']) && $_POST['_rem'] == 1) setcookie("rem", $Args['User']->id, time() + 2592000, '/');

                                $_SESSION["auth_user_ID"] = $Args['AuthUser']->id;
								$_SESSION["sys_name"] = $Args['AuthUser']->sys_name;
								
                                $responseJson['success'] = 1;
                                $responseJson['redirect'] = '/';
							} elseif ($Args['AuthUser'] == -1) $responseJson['errors']['general'] = _('Авторизация неудачна. Попробуйте ещё');
							elseif ($Args['AuthUser'] == -2) $responseJson['errors']['general'] = _('Ваш IP адрес заблокирован');
                        } catch (SiteException $Exc) {}
                    }
                } else {
                    $responseJson['errors']['form']['code'] = _('Код с картинки указан неверно');
                }
			break;
            case 'logout':
                if (isset($Args['User'])) unset($Args['User']);
                if (isset($Args['AuthUser'])) unset($Args['AuthUser']);
                if (isset($_SESSION["auth_user_ID"])) unset($_SESSION["auth_user_ID"]);
                if (isset($_SESSION["sys_name"])) unset($_SESSION["sys_name"]);
					
				$responseJson['success'] = 1;
                $responseJson['redirect'] = '/';
			break;

        }

        print json_encode($responseJson);
        exit;
    }
}

$tmpl = 'login.tpl';

//----------------------------------------------------------------------------------------------------------------------------------------------------
if (isset($_SESSION['auth_user_ID']) && strlen($_SESSION['auth_user_ID'])) {

    try
    {
        $Args['AuthUser'] = new AuthUser(array('id' => $_SESSION['auth_user_ID']));
		$Args['User'] = new User(array('id' => $Args['AuthUser']->sys_userID));
		$_SESSION['sys_name'] = $Args['AuthUser']->sys_name;
        $tmpl = 'index.tpl';

		if (isset($Args['path'][0]) && strlen($Args['path'][0]) > 0) {
			if (is_readable(PREFIX . '.engine/handler/' . $Args['path'][0] . '.php')) require(PREFIX . '.engine/handler/' . $Args['path'][0] . '.php');
		}
    } catch (SiteException $Exc)
    {
		Logger::getInstance()->error('User autherization error.');
    }
	try
	{
		$Args['Config'] = Config::getInstance(PREFIX . '.engine/etc/'.$Args['AuthUser']->sys_name.'.conf');
	} catch (SiteException $Exc)
	{
		Logger::getInstance()->error('Error loading configuration file ('.PREFIX . '.engine/etc/'.$Args['AuthUser']->sys_name.'.conf'.')');
		Misc::basicError(_('Ошибка загрузки конфигурационного файла'));
		exit;
	}
}
//----------------------------------------------------------------------------------------------------------------------------------------------------

$Template = STemplate::getInstance();
$Template->smarty->assign($Args);
$Template->smarty->display($tmpl);
?>