CKEDITOR.plugins.add('standarttext',{
    icons: 'stext',
	init: function(editor){
        var cmd = editor.addCommand('standarttext', {
            exec:function(editor){
                editor.insertHtml( (new Date()).toString() );
            }
        });
        cmd.modes = { wysiwyg : 1, source: 1 };
        editor.ui.addButton('standarttext',{
            label: 'Insert Standart Text',
            command: 'standarttext',
            toolbar: 'insert'
        });
    }
    
});