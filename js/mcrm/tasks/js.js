$(document).ready(function() { 
    $('#i_expiry_date').datetimepicker({
		altField: "#i_expiry_time",
		timeFormat: 'HH:mm',
		dateFormat: 'yy-mm-dd',
		stepHour: 1,
		stepMinute: 5
	});
	//------------------------------------------------------------------------------------
	$( "#datepicker" ).datepicker({
		dateFormat: 'yy-mm-dd',
  		onSelect: function(dateText) {
			var dd = dateText;
			$.post("/ajax/", {action: "get_calendar",date: dd},
				function(response){
					$('#data_list').html(response.responseHTML);
					$('#c_date').html(dd);
				}, "json");
		}
  	});
	//------------------------------------------------------------------------------------
	$('#ffb_user').flexbox('/ajax/', {  
        action: 'get_users',
		paging: {  
            pageSize: 10  
        },
		width: 230,
		method: 'POST',
		onSelect: function(){
			if ($('#ffb_user_hidden').val() > 0) {
				$('#i_userID').val($('#ffb_user_hidden').val());
			}
		},
		onChange: function () {
			if ($('#ffb_user_input').val() == '') {
				$('#i_userID').val('0');
			}
		}
    });
	//------------------------------------------------------------------------------------
});