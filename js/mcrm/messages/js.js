$(document).ready(function() { 
	$('#ffb_user').flexbox('/ajax/', {  
        action: 'get_users',
		paging: {  
            pageSize: 10  
        },
		width: 230,
		method: 'POST',
		onSelect: function(){
			if ($('#ffb_user_hidden').val() > 0) {
				$('#i_touserID').val($('#ffb_user_hidden').val());
			}
		},
		onChange: function () {
			if ($('#ffb_user_input').val() == '') {
				$('#i_touserID').val('0');
			}
		}
    });
	//------------------------------------------------------------------------------------
	$('.rm_del').live('click',function(){
		var did = $(this).attr('id').replace('rm_del_', '');
		$('#i_ccID').val($('#i_ccID').val().replace(did + ',', ''));
		$('#cc_tr_' + did).remove();
		return false;
	});
	//------------------------------------------------------------------------------------
	$('#ffb_cc_user').flexbox('/ajax/', {  
        action: 'get_users',
		paging: {  
            pageSize: 10  
        },
		width: 230,
		method: 'POST',
		onSelect: function(){
			if ($('#ffb_cc_user_hidden').val() > 0) {
				$('#i_cc_user_id').val($('#ffb_cc_user_hidden').val());
			}
			addCCUser($('#i_cc_user_id').val(), this.value);
			this.value = '';
		}
    });
	//------------------------------------------------------------------------------------
	function addCCUser(id, name)
	{
		var cc_ids = $('#i_ccID').val();
		if (cc_ids.indexOf(id+',') == -1) {
			$('#cc_list').append('<tr id="cc_tr_' + id + '"><td>'+name+'</td><td><a href="#" class="rm_del" id="rm_del_' + id + '"><img src="/i/delete.png"></a></td></tr>');
			$('#i_ccID').val(cc_ids + id + ',');
		}
	}	
});