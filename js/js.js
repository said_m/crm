$(document).ready(function() {
						   
	
	//drop down for notifications and logout
	$("a#msg_adrop_sh").click(function() {
		$('#msg_pdrop').hide();
		$('p#msg_pdrop_sh').removeClass("active");
		$(this).toggleClass("active");
		$('#msg_adrop').toggle();
		return false;
	});
	$("p#msg_pdrop_sh").click(function() {
		$('#msg_adrop').hide();
		$('a#msg_adrop_sh').removeClass("active");
		$(this).toggleClass("active");
		$('#msg_pdrop').toggle();
		return false;
	});
	
	//init select substitution for current contact
	$("#selected_contact").select2();
	
	
	//Swap between main and admin menus and init rotation of menu
	//$(".ms").hide();
	//$("#nav_swap a:first").addClass("active").show();
	/*
	$(".ms:first").show().find('ul').carouFredSel({
		circular: false,
		infinite: false,
		prev: {
			button: function() {
			return $(this).parents().find(".prev");
			}
		},
		next: {
			button: function() {
			return $(this).parents().find(".next");
			}
		},
		auto: false,
	});
	*/
	
	$("#nav_swap a").click(function() {
		$("#nav_swap a").removeClass("active"); //Remove any "active" class
		$(this).addClass("active"); //Add "active" class to selected tab
		$(".ms").hide(); //Hide all tab content
		var activeTab = $(this).attr("href"); //Find the rel attribute value to identify the active tab + content
		$(activeTab).show(); //Fade in the active content
		$(activeTab).find('ul').carouFredSel({
			circular: false,
			infinite: false,
			prev: {
				button: function() {
				return $(this).parents().find(".prev");
				}
			},
			next: {
				button: function() {
				return $(this).parents().find(".next");
				}
			},
			auto: false,
		});
		return false;
	});
	
	//drop down for secondary menu
	$("a.sec_slide").click(function() {
		$('a#sec_title').toggle();
		$('a#sec_title_close').toggle();
		$('ul#nav_sec').slideToggle();
		return false;
	});
	
	//drop down for select
	$("a.drop").click(function() {
		$(this).toggleClass("active");
		$(this).next('.drop_popup').toggle();
		return false;
	});
						   
	//Default Action
	$(".tab_content").hide(); //Hide all content
	$(".menu3 a:first").addClass("current").show(); //Activate first tab
	$(".tab_content:first").show(); //Show first tab content
	
	//On Click Event
	$(".menu3 a").click(function() {
		$(".menu3 a").removeClass("current"); //Remove any "active" class
		$(this).addClass("current"); //Add "active" class to selected tab
		$(".tab_content").hide(); //Hide all tab content
		var activeTab = $(this).attr("href"); //Find the rel attribute value to identify the active tab + content
		$(activeTab).fadeIn(); //Fade in the active content
		return false;
	});
	
	
});
