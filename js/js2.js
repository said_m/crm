$(document).ready(function() {
						   
	//centering div
	$(window).resize(function(){
		$('#login_form').css({
		position:'absolute',
		left: ($(window).width() - $('#login_form').outerWidth())/2,
		top: ($(window).height() - $('#login_form').outerHeight())/2
		});
	});
	$(window).resize();
	
	//drop down for notifications and logout
	$("a#msg_adrop_sh").click(function() {
		$('#msg_pdrop').hide();
		$('p#msg_pdrop_sh').removeClass("active");
		$(this).toggleClass("active");
		$('#msg_adrop').toggle();
		return false;
	});
	$("p#msg_pdrop_sh").click(function() {
		$('#msg_adrop').hide();
		$('a#msg_adrop_sh').removeClass("active");
		$(this).toggleClass("active");
		$('#msg_pdrop').toggle();
		return false;
	});
	
	//init select substitution for current contact
	$("#selected_contact").select2();
	
	
	//drop down for secondary menu
	$("a#sec_title").click(function() {
		$('ul#nav_sec').slideToggle();
		$(this).find('span').toggle();
		$(this).toggleClass('active');
		$(this).text('Close');
		return false;
	});

	
	
	
	
	
	
	//drop down for select
	$("a.drop").click(function() {
		$(this).toggleClass("active");
		$(this).next('.drop_popup').toggle();
		return false;
	});
	
						   
	//Default Action
	$(".tab_content").hide(); //Hide all content
	$(".menu3 a:first").addClass("current").show(); //Activate first tab
	$(".tab_content:first").show(); //Show first tab content
	
	//On Click Event
	$(".menu3 a").click(function() {
		$(".menu3 a").removeClass("current"); //Remove any "active" class
		$(this).addClass("current"); //Add "active" class to selected tab
		$(".tab_content").hide(); //Hide all tab content
		var activeTab = $(this).attr("href"); //Find the rel attribute value to identify the active tab + content
		$(activeTab).fadeIn(); //Fade in the active content
		return false;
	});

});
