
$(document).ready(function() { 
    /*
     * new design addon
    */
    //--------------------------------------------------------------------------
	$('#i_h_logout, #i_b_logout').bind('click', function(){
        $.post("/", {
            action: "logout"
        }, function(response){
            location.replace('/');
        }, "json");
        return false;
    });    
	
	$('a.action').live('click', bindInputButtons);    
    


    $('form').ajaxForm({
        //iframe: false,
        dataType: 'json',
        beforeSubmit: function (formData, jqForm, options){
            
            var action = $.grep(formData, function(obj){
                return (obj.name == 'action');
            });
            var form = ($('#i_' + action[0].value + '_form').length) ? $('#i_' + action[0].value + '_form') : $(document);
            
            form.find('div.formError').remove();
            $("#i_loading_dialog").dialog({
                resizable: false,
                width: 400,
                height: 200,
                modal: true,
                closeOnEscape: false,
                draggable: false,
                resizable: false,
                open: function(event, ui) {
                    $(".ui-dialog-titlebar-close").hide();
                }
            });
            if (eval("typeof uploadProgressStart == 'function'")) {
                uploadProgressStart();
            }
        },
        success: parseFormResponse
    });
	
	
    if ($("#i_date").length > 0 && $("#i_date").length > 0) {
        $("#i_date").datepicker({
            dateFormat: $.datepicker.W3C,
            changeMonth: false,
            showStatus: true,
            showOn: "both",
            buttonImage: "/i/calendar.gif",
            buttonImageOnly: true ,
            firstDay: 1
        }).attr("readonly", "readonly");
    }
	
 	$(function() {
		$('#tree1').tree();
	});
	$('#tree1').bind(
		'tree.click',
		function(event) {
			// The clicked node is 'event.node'
			var node = event.node;
			var url = $('#tree1').attr('data-url').replace('tree', 'list/' + node.id);
			$.ajax({
                    type: 'GET',
                    url: url,
                    dataType: 'json',
                    success: function(response){
						$('#data_list').html(response.responseHTML).fadeIn(600);
                    }
			});

		}
	);
	
	$.datepicker.regional['ru'] = {
		closeText: 'Закрыть',
		prevText: '<Пред',
		nextText: 'След>',
		currentText: 'Сегодня',
		monthNames: ['Январь','Февраль','Март','Апрель','Май','Июнь',
		'Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
		monthNamesShort: ['Янв','Фев','Мар','Апр','Май','Июн',
		'Июл','Авг','Сен','Окт','Ноя','Дек'],
		dayNames: ['воскресенье','понедельник','вторник','среда','четверг','пятница','суббота'],
		dayNamesShort: ['вск','пнд','втр','срд','чтв','птн','сбт'],
		dayNamesMin: ['Вс','Пн','Вт','Ср','Чт','Пт','Сб'],
		weekHeader: 'Не',
		dateFormat: 'dd.mm.yy',
		firstDay: 1,
		isRTL: false,
		showMonthAfterYear: false,
		yearSuffix: ''
	};
	$.datepicker.setDefaults($.datepicker.regional['ru']);
   
}); 





function login_form(resp)
{
    if (resp.success == 1 && resp.redirect)
    {
        location.replace(resp.redirect);
    }
}

function toggleDivTag(id) {
    var divID = document.getElementById(id);
    if(divID.style.display == 'block')
        divID.style.display = 'none';
    else
        divID.style.display = 'block';

}
function parseFormResponse(response){
    
	if (eval("typeof uploadProgressFinish == 'function'")) {
        uploadProgressFinish();
    }
    
    if ($('#cimg').length > 0){
        var rnd = new Date().getTime();
        $('#cimg').attr('src', $('#cimg').attr('src') + '?' + rnd);
    }

	//var form = $('#i_' + response.action + '_form');
    var form = ($('#i_' + response.action + '_form').length) ? $('#i_' + response.action + '_form') : $(document);
    form.find('input[type=submit]').attr('disabled', '');

    if (response.success == 0 && response.errors)
    {
        $("#i_loading_dialog").dialog('destroy');
        if (response.errors.general)
        {

            //showError(form, form, response.errors.general);
            //var errOffset = errInput.offset();
            
            var errInput = form;
            
            var errOffset = errInput.position();
            
            
            callerTopPosition = errOffset.top;
            callerleftPosition = errOffset.left;
            callerWidth =  errInput.width()

            var divFormError = document.createElement('div');
            var formErrorContent = document.createElement('div');

            $(divFormError).addClass("formError");
            $(formErrorContent).addClass("formErrorContent")

            form.append(divFormError);
            $(divFormError).append(formErrorContent);

            $(formErrorContent).html(response.errors.general);
            inputHeight = $(divFormError).height();
            callerleftPosition +=  callerWidth - callerWidth;
            callerTopPosition += -inputHeight -10;
            $(divFormError).css({
                top:callerTopPosition,
                left:callerleftPosition,
                opacity:0
            });
            $(divFormError).animate({
                "opacity":0.87
            },function(){
                return true;
            });
            
        }
        if (response.errors.form)
        {
            $.each(response.errors.form, function(k, v){
				var errInput = form.find('#i_'+k);
				showError(form, errInput, v);
            });
        }

    }else
    {
        if (response.success == 1 && response.redirect)
        {
            /*
            $.blockUI({
                css: {
                    border: 'none',
                    padding: '15px',
                    backgroundColor: '#000',
                    '-webkit-border-radius': '10px',
                    '-moz-border-radius': '10px',
                    opacity: .5,
                    color: '#fff'
                }
            });
            */
            var tId=null;
            tId = window.setTimeout("location.replace('"+response.redirect+"')",900);

        }else{
            $("#i_loading_dialog").dialog('destroy');
        }
    }
    
    try
    {
        if (response.formid) window[response.formid](response);
        else window[response.action](response);
    }catch(err){}

//console.debug(response);
}

function showError(appTo, el, errStr){
    callerTopPosition = el.parent().position().top;
    callerleftPosition = el.parent().position().left;
    callerWidth =  el.width()

	var divFormError = $('<div class="formError"></div>')
    .append('<div class="formErrorArrow"><div class="line10" style="margin: 0 auto"><!-- --></div><div class="line9" style="margin: 0 auto"><!-- --></div><div class="line8" style="margin: 0 auto"><!-- --></div><div class="line7" style="margin: 0 auto"><!-- --></div><div class="line6" style="margin: 0 auto"><!-- --></div><div class="line5" style="margin: 0 auto"><!-- --></div><div class="line4" style="margin: 0 auto"><!-- --></div><div class="line3" style="margin: 0 auto"><!-- --></div><div class="line2" style="margin: 0 auto"><!-- --></div><div class="line1" style="margin: 0 auto"><!-- --></div></div>')
    .append('<div class="formErrorContent">'+errStr+'</div>')
    .appendTo(appTo)
    .css({
        top: function(idx, value){
			return callerTopPosition-15;// - $(this).height();
        },
        left: callerleftPosition + 20,
        opacity:0
    })
    .animate({
        "opacity":0.87
    },function(){
        return true;
    });
}

function listSplit( val ) {
    return val.split( /,\s*/ );
}
function listExtractLast( term ) {
    return listSplit( term ).pop();
}

function listCheckComa(el){
    var tmp = $.trim(el.val());
    if (tmp.length && tmp.substring(0, tmp.length-1) != ','){
        tmp += ', ';
        el.val(tmp);
    }
}


function bindInputButtons(e){
	e.stopPropagation();
    if ($(this).attr('ref')){
        location.replace($(this).attr('ref'));
    }else{
        $($(this).parents('form')[0]).trigger('submit');
        //return false;
    }
}

$(function() {
    $( "#dialog:ui-dialog" ).dialog( "destroy" );
    
	$('.delete_object').click(function(){
        var did = $(this).attr('rel').replace('del_', '');
		$( "#dialog-confirm" ).dialog({
            resizable: false,
            modal: true,
            buttons: {
                "Удалить": function() {
                    $('#i_del').val(did);
                    $('#i_delete_form').trigger('submit');
                    $( this ).dialog( "close" );
                },
                "Отмена": function() {
                    $( this ).dialog( "close" );
                }
            }
        });
    });

});
