<?php
define("PREFIX", dirname($_SERVER['SCRIPT_FILENAME']) . '/');
include_once(PREFIX . '.engine/lib/Captcha.class.php');
$img = new Captcha(
	array(
		'fonts_dir'=>PREFIX . '.engine/lib/fonts/', 
		'text_minimum_distance'=>26, 
		'text_maximum_distance'=>30, 
		'font_size'=>18, 
		'image_height'=>30, 
		'image_width'=>120
		));
$img->show();
exit;
?>