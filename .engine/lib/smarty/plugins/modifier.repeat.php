<?php
function smarty_modifier_repeat($string, $num)
{
    return str_repeat($string, $num);
}

/* vim: set expandtab: */

?>
