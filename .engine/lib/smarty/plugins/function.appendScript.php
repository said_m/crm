<?php
function smarty_function_appendScript($params, &$smarty)
{
    if (isset($params['src'])) {
        $needFolderPrefix = isset($params['needFolderPrefix']) ? $params['needFolderPrefix'] : true;
        $prio = isset($params['prio']) ? $params['prio'] : 0;
        $ext = isset($params['ext']) ? $params['ext'] : null;
        Helper_View_Append::getInstance()->appendScript($params['src'], $needFolderPrefix, $prio, $ext);
    }
}