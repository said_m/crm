<?php

/**
 * Smarty plugin
 * 
 * @package Smarty
 * @subpackage PluginsFunction
 */

/**
 * Smarty {html_options_obj} function plugin
 * 
 * Type:     function<br>
 * Name:     html_options_obj<br>
 * Purpose:  Prints the list of <option> tags generated from
 *           the passed parameters<br>assuming that object has get_id() and get_title() functions for option value and title 
 * Params:
 * <pre>
 * - name       (optional) - string default "select"
 * - options    (required) - if no values supplied) - associative array
 * - selected   (optional) - string default not set or array for multiple select
 * - id         (optional) - string default not set
 * - class      (optional) - string default not set
 * - multiple   (optional) true or false for multiple select
 * </pre>
 * 
 * 
 *      (Smarty online manual)
 * @author Yury
 * @param array                    $params   parameters
 * @param Smarty_Internal_Template $template template object
 * @return string 
 * @uses smarty_function_escape_special_chars()
 */
function smarty_function_html_options_obj($params, $template) {
    require_once(SMARTY_PLUGINS_DIR . 'shared.escape_special_chars.php');

    $name = null;
    $options = null;
    $selected = null;
    $output = null;
    $id = null;
    $class = null;
    $extra = '';

    foreach ($params as $_key => $_val) {
        switch ($_key) {
            case 'name':
            case 'class':
            case 'id':
                $$_key = (string) $_val;
                break;

            case 'options':
                $options = (array) $_val;
                break;

            case 'selected':
                if (is_array($_val)) {
                    $selected = array();
                    foreach ($_val as $_sel) {
                        $_sel = $_sel->get_id();
                        $selected[$_sel] = true;
                    }
                } elseif (is_object($_val)) {
                    $selected = $_val->get_id();
                } else {
                    trigger_error("html_options_obj: selected attribute should be object or array of objects", E_USER_NOTICE);
                }
                break;

            default:
                if (!is_array($_val)) {
                    $extra .= ' ' . $_key . '="' . smarty_function_escape_special_chars($_val) . '"';
                } else {
                    trigger_error("html_options: extra attribute '$_key' cannot be an array", E_USER_NOTICE);
                }
                break;
        }
    }

    if (!isset($options) && !isset($values)) {
        /* raise error here? */
        return '';
    }

    $_html_result = '';
    $_idx = 0;

    if (isset($options)) {
        foreach ($options as $_obj) {
            $_html_result .= smarty_function_html_options_optoutput_obj($_obj, $selected, $id, $class, $_idx);
        }
    }

    if (!empty($name)) {
        $_html_class = !empty($class) ? ' class="' . $class . '"' : '';
        $_html_id = !empty($id) ? ' id="' . $id . '"' : '';
        $_html_result = '<select name="' . $name . '"' . $_html_class . $_html_id . $extra . '>' . "\n" . $_html_result . '</select>' . "\n";
    }

    return $_html_result;
}

function smarty_function_html_options_optoutput_obj($obj, $selected, $id, $class, &$idx) {

    $_key = smarty_function_escape_special_chars($obj->get_id());
    $_html_result = '<option value="' . $_key . '"';
    if (is_array($selected)) {
        if (isset($selected[$_key])) {
            $_html_result .= ' selected="selected"';
        }
    } elseif ($_key === $selected) {
        $_html_result .= ' selected="selected"';
    }
    $_html_class = !empty($class) ? ' class="' . $class . ' option"' : '';
    $_html_id = !empty($id) ? ' id="' . $id . '-' . $idx . '"' : '';

    $_html_result .= $_html_class . $_html_id . '>' . $obj->get_title() . '</option>' . "\n";
    $idx++;

    return $_html_result;
}

?>