<?php
/**
 * Based on "Securimage" http://www.phpcaptcha.org
*/

class Captcha 
{
  public $image_width = 150;
  public $image_height = 40;
  // 1 - jpeg, 2 - png, 3 - gif
  public $image_type = 1;
  public $code_length = 4;
  public $charset = 'ABCDEFGHKLMNPRSTUVWYZ23456789';
  public $fonts_dir = 'fonts';
  public $ttf_file = "antquab.ttf";
  public $ttf_random_font = true;
  public $font_size = 20;
  public $text_angle_minimum = -20;
  public $text_angle_maximum = 20;
  public $text_x_start = 8;
  public $text_minimum_distance = 30;
  public $text_maximum_distance = 39;
  public $image_bg_color = "#F2E2C4";
  public $text_color = "#303838";
  public $use_multi_text = true;
  public $multi_text_color = "#0D0D0D,#594828,#59594C";
  public $use_transparent_text = true;
  public $text_transparency_percentage = 15;
  public $draw_lines = true;
  public $line_color = "#BFA980";
  public $line_distance = 10;
  public $line_thickness = 1;
  public $draw_angled_lines = false;
  public $draw_lines_over_text = false;
  public $arc_linethrough = false;
  public $arc_line_colors = "#8080ff";

  private $im;
  private $bgimg;
  private $code;
  private $code_entered;
  private $correct_code;

  function __construct($Params = array()) 
  {
  	if ( session_id() == '' ) session_start();
	  if (count($Params)) 
	  {
		  foreach ($Params as $k=>$v) $this->$k = $v;
	  }
  	
  }
  function show($background_image = "")
  {
    if($background_image != "" && is_readable($background_image)) {
      $this->bgimg = $background_image;
    }
    $this->doImage();
  }
  function check($code)
  {
    $this->code_entered = $code;
    $this->validate();
    return $this->correct_code;
  }
  function doImage()
  {
    if($this->use_transparent_text == true || $this->bgimg != "") {
      $this->im = imagecreatetruecolor($this->image_width, $this->image_height);
      $bgcolor = imagecolorallocate($this->im, hexdec(substr($this->image_bg_color, 1, 2)), hexdec(substr($this->image_bg_color, 3, 2)), hexdec(substr($this->image_bg_color, 5, 2)));
      imagefilledrectangle($this->im, 0, 0, imagesx($this->im), imagesy($this->im), $bgcolor);
    } else { //no transparency
      $this->im = imagecreate($this->image_width, $this->image_height);
      $bgcolor = imagecolorallocate($this->im, hexdec(substr($this->image_bg_color, 1, 2)), hexdec(substr($this->image_bg_color, 3, 2)), hexdec(substr($this->image_bg_color, 5, 2)));
    }

    if($this->bgimg != "") { $this->setBackground(); }

    $this->createCode();

    if (!$this->draw_lines_over_text && $this->draw_lines) $this->drawLines();

    $this->drawWord();

    if ($this->arc_linethrough == true) $this->arcLines();

    if ($this->draw_lines_over_text && $this->draw_lines) $this->drawLines();

    $this->output();

  }

  private function setBackground()
  {
    $dat = @getimagesize($this->bgimg);
    if($dat == false) { return; }

    switch($dat[2]) {
      case 1:  $newim = @imagecreatefromgif($this->bgimg); break;
      case 2:  $newim = @imagecreatefromjpeg($this->bgimg); break;
      case 3:  $newim = @imagecreatefrompng($this->bgimg); break;
      case 15: $newim = @imagecreatefromwbmp($this->bgimg); break;
      case 16: $newim = @imagecreatefromxbm($this->bgimg); break;
      default: return;
    }

    if(!$newim) return;

    imagecopy($this->im, $newim, 0, 0, 0, 0, $this->image_width, $this->image_height);
  }

  private function arcLines()
  {
    $colors = explode(',', $this->arc_line_colors);
    imagesetthickness($this->im, 3);

    $color = $colors[rand(0, sizeof($colors) - 1)];
    $linecolor = imagecolorallocate($this->im, hexdec(substr($color, 1, 2)), hexdec(substr($color, 3, 2)), hexdec(substr($color, 5, 2)));

    $xpos   = $this->text_x_start + ($this->font_size * 2) + rand(-5, 5);
    $width  = $this->image_width / 2.66 + rand(3, 10);
    $height = $this->font_size * 2.14 - rand(3, 10);

    if ( rand(0,100) % 2 == 0 ) {
      $start = rand(0,66);
      $ypos  = $this->image_height / 2 - rand(5, 15);
      $xpos += rand(5, 15);
    } else {
      $start = rand(180, 246);
      $ypos  = $this->image_height / 2 + rand(5, 15);
    }

    $end = $start + rand(75, 110);

    imagearc($this->im, $xpos, $ypos, $width, $height, $start, $end, $linecolor);

    $color = $colors[rand(0, sizeof($colors) - 1)];
    $linecolor = imagecolorallocate($this->im, hexdec(substr($color, 1, 2)), hexdec(substr($color, 3, 2)), hexdec(substr($color, 5, 2)));

    if ( rand(1,75) % 2 == 0 ) {
      $start = rand(45, 111);
      $ypos  = $this->image_height / 2 - rand(5, 15);
      $xpos += rand(5, 15);
    } else {
      $start = rand(200, 250);
      $ypos  = $this->image_height / 2 + rand(5, 15);
    }

    $end = $start + rand(75, 100);

    imagearc($this->im, $this->image_width * .75, $ypos, $width, $height, $start, $end, $linecolor);
  }


  private function drawLines()
  {
    $linecolor = imagecolorallocate($this->im, hexdec(substr($this->line_color, 1, 2)), hexdec(substr($this->line_color, 3, 2)), hexdec(substr($this->line_color, 5, 2)));
    imagesetthickness($this->im, $this->line_thickness);

    //vertical lines
    for($x = 1; $x < $this->image_width; $x += $this->line_distance) {
      imageline($this->im, $x, 0, $x, $this->image_height, $linecolor);
    }

    //horizontal lines
    for($y = 11; $y < $this->image_height; $y += $this->line_distance) {
      imageline($this->im, 0, $y, $this->image_width, $y, $linecolor);
    }

    if ($this->draw_angled_lines == true) {
      for ($x = -($this->image_height); $x < $this->image_width; $x += $this->line_distance) {
        imageline($this->im, $x, 0, $x + $this->image_height, $this->image_height, $linecolor);
      }

      for ($x = $this->image_width + $this->image_height; $x > 0; $x -= $this->line_distance) {
        imageline($this->im, $x, 0, $x - $this->image_height, $this->image_height, $linecolor);
      }
    }
  }

  private function drawWord()
  {
     //ttf font
      if($this->use_transparent_text == true) {
        $alpha = intval($this->text_transparency_percentage / 100 * 127);
        $font_color = imagecolorallocatealpha($this->im, hexdec(substr($this->text_color, 1, 2)), hexdec(substr($this->text_color, 3, 2)), hexdec(substr($this->text_color, 5, 2)), $alpha);
      } else { //no transparency
        $font_color = imagecolorallocate($this->im, hexdec(substr($this->text_color, 1, 2)), hexdec(substr($this->text_color, 3, 2)), hexdec(substr($this->text_color, 5, 2)));
      }

      $x = $this->text_x_start;
      $strlen = strlen($this->code);
      $y_min = ($this->image_height / 2) + ($this->font_size / 2) - 2;
      $y_max = ($this->image_height / 2) + ($this->font_size / 2) + 2;
      $colors = explode(',', $this->multi_text_color);

      for($i = 0; $i < $strlen; ++$i) {
        $angle = rand($this->text_angle_minimum, $this->text_angle_maximum);
        $y = rand($y_min, $y_max);
        if ($this->use_multi_text == true) {
          $idx = rand(0, sizeof($colors) - 1);
          $r = substr($colors[$idx], 1, 2);
          $g = substr($colors[$idx], 3, 2);
          $b = substr($colors[$idx], 5, 2);
          if($this->use_transparent_text == true) {
            $font_color = imagecolorallocatealpha($this->im, "0x$r", "0x$g", "0x$b", $alpha);
          } else {
            $font_color = imagecolorallocate($this->im, "0x$r", "0x$g", "0x$b");
          }
        }
        if ($this->ttf_random_font)
        {
        	$fonts = array();
			foreach (new DirectoryIterator($this->fonts_dir) as $fileInfo) 
			{
			    $fname = $fileInfo->getFilename();
				if(substr($fname, -3) != 'ttf') continue;
			    $fonts[] = $fileInfo->getFilename();
			}
			mt_srand((double)microtime()*1000000);
		    $rand = mt_rand(0, count($fonts)-1);
			$font = $fonts[$rand];
			@imagettftext($this->im, $this->font_size, $angle, $x, $y, $font_color, $this->fonts_dir . $font, $this->code{$i});
        }else @imagettftext($this->im, $this->font_size, $angle, $x, $y, $font_color, $this->fonts_dir . $this->ttf_file, $this->code{$i});

        $x += rand($this->text_minimum_distance, $this->text_maximum_distance);
      } //for loop
     //else ttf font
  } //function

  
  function createCode()
  {
    $this->code = false;
	$this->code = $this->generateCode($this->code_length);
    $this->saveData();
  }
  private function generateCode($len)
  {
    $code = '';
    for($i = 1, $cslen = strlen($this->charset); $i <= $len; ++$i) {
      $code .= strtoupper( $this->charset{rand(0, $cslen - 1)} );
    }
    return $code;
  }

  function output()
  {
    header("Expires: Sun, 1 Jan 2000 12:00:00 GMT");
    header("Last-Modified: " . gmdate("D, d M Y H:i:s") . "GMT");
    header("Cache-Control: no-store, no-cache, must-revalidate");
    header("Cache-Control: post-check=0, pre-check=0", false);
    header("Pragma: no-cache");

    switch($this->image_type)
    {
      case 1:
        header("Content-Type: image/jpeg");
        imagejpeg($this->im, null, 90);
        break;

      case 3:
        header("Content-Type: image/gif");
        imagegif($this->im);
        break;

      default:
        header("Content-Type: image/png");
        imagepng($this->im);
        break;
    }

    imagedestroy($this->im);
  }

  private function saveData()
  {
    $_SESSION['securimage_code_value'] = strtolower($this->code);
  }

  function validate()
  {
    if ( isset($_SESSION['securimage_code_value']) && !empty($_SESSION['securimage_code_value']) ) {
      if ( $_SESSION['securimage_code_value'] == strtolower(trim($this->code_entered)) ) {
        $this->correct_code = true;
        $_SESSION['securimage_code_value'] = '';
      } else {
        $this->correct_code = false;
      }
    } else {
      $this->correct_code = false;
    }
  }
  function getCode()
  {
    if (isset($_SESSION['securimage_code_value']) && !empty($_SESSION['securimage_code_value'])) {
      return $_SESSION['securimage_code_value'];
    } else {
      return '';
    }
  }

  function checkCode()
  {
    return $this->correct_code;
  }
}
?>