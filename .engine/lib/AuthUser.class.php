<?php
class AuthUser extends AuthMapper
 {
    const DBTABLE = 'auth_users'; 
	protected static $table = 'auth_users';
	public $class_title;
    function __construct($Params = array(), $init = 0, $create = 0)  
     {
      
	  	if ($create == 1) $this->create($Params);
      	else parent::__construct($Params, $init);
		
        $this->class_title = $this->login;  
	 }
   function __destruct()
    {
     
    }
	 
//------------------------------------------------------------------------------- 
public static function get($Params = array())
{
	return parent::get($Params);
} 
//------------------------------------------------------------------------------------------------------------     
public static function count($Params = array())
{
	return parent::count($Params);
}
//------------------------------------------------------------------------------------------------------------   
public static function delete($Params = array())
{
	return parent::delete($Params);
}
//------------------------------------------------------------------------------------------------------------  
public static function authorize($login, $password) {
	//Intruder::clear();
        
	$HTTP_X_FORWARDED_FOR = (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : '';
	
	$Intruder = Intruder::check(array('remoteIP' => $_SERVER['REMOTE_ADDR'], 'proxyIP' => $HTTP_X_FORWARDED_FOR));
	if ($Intruder != false && $Intruder->quantity >= 20) {
		$Intruder->increment();
		Logger::getInstance()->error("Authorization failed (User is blocked): [Login: $login; Password: $password;]\nHTTP_X_FORWARDED_FOR: " . $_SERVER['HTTP_X_FORWARDED_FOR'] . ";\nREMOTE_ADDR: " . $_SERVER['REMOTE_ADDR'] . ";\n");
		return -2;
	}

	$Res = AuthUser::getOne(array('where' => array('login' => $login, 'password' => $password)));
	Logger::getInstance()->debug(print_r($Res, true));
	if ($Res != false) return $Res;

	if ($Intruder == false) {
		try {
			$Intruder = new Intruder(array('remoteIP' => $_SERVER['REMOTE_ADDR'], 'proxyIP' => $HTTP_X_FORWARDED_FOR, 'mtime' => date('y-m-d H:i:s'), 'quantity' => 0), 0, 1);
		} catch (SiteException $Exc) {}
	}
	$Intruder->increment();
	Logger::getInstance()->error("Authorization failed: [Login: $login; Password: $password;]\nHTTP_X_FORWARDED_FOR: " . $HTTP_X_FORWARDED_FOR . ";\nREMOTE_ADDR: " . $_SERVER['REMOTE_ADDR'] . ";\n");
	return -1;
    }

//-----------------------------------------------------------------------------------
 
 }
?>