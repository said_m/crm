<?php
require_once(SMARTY_DIR . 'Smarty.class.php');
class STemplate
{
    static private $instance = NULL;

    static function getInstance()
    {
        if (self::$instance == NULL)
        {
            self::$instance = new STemplate();
        }
        return self::$instance;
    }

    protected function __construct()
    {
        $this->smarty = new Smarty();
        //$Config = Config::getInstance();
        $this->smarty->compile_dir = PREFIX . '.engine/var/compile/';
        $this->smarty->template_dir = PREFIX . '.engine/templates/';
    }

    private function __clone()
    {

    }
}
?>
