<?php

class DBConnectionAuth
{

    static private $instance = array('read' => NULL, 'write' => NULL);

    static function getInstance($type = 'write')
    {
        if (self::$instance[$type] == NULL) {
            self::$instance[$type] = new DBConnectionAuth($type);
        }
        
        return self::$instance[$type];
    }

    private function __construct($type)
    {
        $Config = Config::getInstance();
        //Logger::getInstance()->debug(print_r($Config, true));
		if ($type == 'read') {
			$this->DBLink = new MySQLi2(
				$Config->getConf()->database->host_read, 
				$Config->getConf()->database->login, 
				$Config->getConf()->database->pass, 
				$Config->getConf()->database->dbname
				);
        } else {
			$this->DBLink = new MySQLi2(
				$Config->getConf()->database->host_write, 
				$Config->getConf()->database->login, 
				$Config->getConf()->database->pass, 
				$Config->getConf()->database->dbname
				);
    	}
		
		if (mysqli_connect_errno()) throw new SiteException(SiteException::DATABASE_FAILED, 'Database connection failed' . mysqli_connect_error(), __FILE__, __LINE__);
		$this->DBLink->query("SET CHARACTER SET UTF8");
		$this->DBLink->autocommit(TRUE);
    }

    private function __clone()
    {

    }

    //--------------------------------------------------------------------------------------------
}

?>