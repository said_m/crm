<?php

/*
  `remoteIP`
  `proxyIP`
  `quantity`
  `mtime`
 */

class Intruder extends AuthMapper {

//------------------------------------------------------------------------------------------------------------- 
    protected static $table = 'auth_login_attempts';

    function __construct($Params = array(), $init = 0, $create = 0) {
        if ($create == 1)
            $this->create($Params);
        else
            parent::__construct($Params, $init);
    }

    //-------------------------------------------------------------------------------------------------------------
    static public function check($Params = array()) {
        $Res = Intruder::get(array('where' => $Params));
        if ($Res)
            return $Res[0];
        else
            return false;
    }

    //-------------------------------------------------------------------------------------------------------------
    public function increment() {
		if (!isset($this->quantity)) $this->quantity = 0;
        $this->edit(array('quantity' => ($this->quantity + 1), 'mtime' => date('y-m-d H:i:s')));
    }

    //-------------------------------------------------------------------------------------------------------------
    static public function clear() {
        try {
            $Connection = DBConnectionAuth::getInstance();
        } catch (SiteException $Exc) {
            return false;
        }
		if ($Connection->DBLink->query('DELETE FROM ' . static::$table . " WHERE (UNIX_TIMESTAMP()-UNIX_TIMESTAMP(mtime))/60 > 240")) return true;
    }

    //-------------------------------------------------------------------------------------------------------------
}

?>