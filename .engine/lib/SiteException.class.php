<?php
class SiteException extends Exception 
{
	protected $url;
	public function __construct ($errstr, $errno = 0, $errfile = '', $errline = 0) 
	{
		parent::__construct();
		$this->code = $errno;
		$this->message = $errstr;
		$this->file = $errfile;
		$this->line = $errline;
		$this->url = $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'];
		$this->registerError();
	}
//-------------------------------------------------------------------
private function registerError()
{
	Logger::getInstance()->exception($this);
	
}
//-------------------------------------------------------------------
public function getUrl()
{
	return $this->url;
}
//-------------------------------------------------------------------
    
}
?>