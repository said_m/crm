<?php

class Paginator {
    public $current = 0;
    public $limit = 20;
    public $skip = 0;
    public $offset = 0;
    public $pages = array();
    public $pqty = 0;
//------------------------------------------------------------------------------------------------------------- 
    function __construct($total = 0, $ItemsPerPage = 20, $rpath = array(), $query_string = '') {
        
        $currIdx = 0;
        $rpath = array_reverse($rpath);
        if (substr($rpath[0], 0, 5) == 'page-') {
            $tmp = explode('-', $rpath[0]);
            $currIdx = $tmp[1];
            unset($rpath[0]);
        }

        $rpath = array_reverse($rpath);
        
        $this->current = $currIdx;
        $this->limit = $ItemsPerPage;
        $this->offset = $this->skip = $currIdx * $ItemsPerPage;
        $pages = ceil($total / $ItemsPerPage);
        if ($pages > 1) {
            $leftIdx = 0;
            $rightIdx = 0;

            if (($currIdx - 4) <= 0) {
                $leftIdx = 0;
                $rightIdx = 6;
                if ($rightIdx > $pages - 1)
                    $rightIdx = $pages - 1;
            }else if (($currIdx + 4) > $pages) {
                $leftIdx = $pages - 7;
                $rightIdx = $pages - 1;
            } else {
                $leftIdx = $currIdx - 3;
                $rightIdx = $currIdx + 3;
            }

            if ($currIdx > 0)
                $this->prev = '/' . implode('/', $rpath) . '/page-' . ($currIdx - 1) . '/' . $query_string;
            if ($currIdx < $pages - 1)
                $this->next = '/' . implode('/', $rpath) . '/page-' . ($currIdx + 1) . '/' . $query_string;
            //$this->pages = array();
            for ($i = $leftIdx; $i <= $rightIdx; $i++)
                $this->pages["$i"] = '/' . implode('/', $rpath) . '/page-' . $i . '/' . $query_string;
        }
        $this->pqty = count($this->pages);
        
    }

//------------------------------------------------------------------------------------------------------------
}

?>