<?php
/**
 * 
 */
interface Helper_View_Interface
{
    /**
     * this method has to retrun ready html of viewhelper
     * 
     */
    public function draw();
}