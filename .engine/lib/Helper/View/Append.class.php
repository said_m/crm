<?php
class Helper_View_Append implements Helper_View_Interface
{
    /**
     * @var Helper_View_Append
     */
    static protected $_instance;
    protected $_scripts;
    protected $_scriptsPrio;
    protected $_scriptsNeedPrefix;
    protected $_scriptsExt;
    protected $_scriptsFolderPrefix;
    protected $_styles;
    protected $_stylesPrio;
    protected $_stylesNeedPrefix;
    protected $_stylesExt;
    protected $_stylesFolderPrefix;
    
    /**
     * constructor
     */
    public function __construct()
    {
        $this->_init();
    }
    
    /**
     * append new script to storage from $src name
     * set $needFolderPrefix to false if you use your own folder prefix
     * set more prio to up your script in order (ususally system scripts have prio 1000)
     * set your own extension ('js' by default), you can set empty string $ext to do not use any extension
     * 
     * @param string $src
     * @param boolean $needFloderPrefix
     * @param int $prio
     * @param string $ext
     * @return string
     */
    public function appendScript($src, $needFolderPrefix = true, $prio = 0, $ext = null)
    {
        $key = array_search($src, $this->_scripts);
        if ($key === false) {
            // add script
            $this->_scripts[] = $src;
            $this->_scriptsPrio[] = $prio;
            $this->_scriptsNeedPrefix[] = $needFolderPrefix;
            $this->_scriptsExt[] = $ext;
        } else {
            // check and maybe change prio
            if ($this->_scriptsPrio[$key] < $prio) {
                $this->_scriptsPrio[$key] = $prio;
            }
        }
    }
    
    /**
     * append new style to storage from $src name
     * set $needFolderPrefix to false if you use your own folder prefix
     * set more prio to up your style in order (ususally system styles have prio 1000)
     * set your own extension ('css' by default), you can set empty string $ext to do not use any extension
     * 
     * @param string $src
     * @param boolean $needFloderPrefix
     * @param int $prio
     * @param string $ext
     * @return string
     */
    public function appendStyle($src, $needFolderPrefix = true, $prio = 0, $ext = null)
    {
        $key = array_search($src, $this->_styles);
        if ($key === false) {
            // add style
            $this->_styles[] = $src;
            $this->_stylesPrio[] = $prio;
            $this->_stylesNeedPrefix[] = $needFolderPrefix;
            $this->_stylesExt[] = $ext;
        } else {
            // check and maybe change prio
            if ($this->_stylesPrio[$key] < $prio) {
                $this->_stylesPrio[$key] = $prio;
            }
        }
    }
    
    /**
     * set folder prefix where your default js files are placed
     * 
     * @example setScriptsFolderPrefix('/js/');
     * @param string $prefix
     */
    public function setScriptsFolderPrefix($prefix)
    {
        $this->_scriptsFolderPrefix = $prefix;
    }
    
    /**
     * set folder prefix where your default css files are placed
     * 
     * @example setStylesFolderPrefix('/css/');
     * @param string $prefix
     */
    public function setStylesFolderPrefix($prefix)
    {
        $this->_stylesFolderPrefix = $prefix;
    }
    
    /**
     * set folders prefixex where your default js and css files are placed
     * 
     * @example setFolderPrefixes('/js/' ,'/css/');
     * @param string $prefix
     */
    public function setFolderPrefixes($scriptPrefix, $stylePrefix)
    {
        $this->_scriptsFolderPrefix = $scriptPrefix;
        $this->_stylesFolderPrefix = $stylePrefix;
    }
    
    /**
     * @return Helper_View_Append
     */
    public static function getInstance()
    {
        if (self::$_instance === null) {
            self::$_instance = new Helper_View_Append();
        }
        return self::$_instance;
    }
    
    /**
     * init default data (used by constructor)
     */
    protected function _init()
    {
        $this->_scripts = array();
        $this->_scriptsPrio = array();
        $this->_scriptsNeedPrefix = array();
        $this->_scriptsExt = array();
        $this->_styles = array();
        $this->_stylesPrio = array();
        $this->_stylesNeedPrefix = array();
        $this->_stylesExt = array();
    }
    
    /**
     * returns html of storage sorted by prio and chronology
     * 
     * @return string
     */
    public function draw()
    {
        $html = '';
        
        // scripts
        $keys = $this->_getScriptsSortedKeys();
        foreach ($keys as $value) {
            $src = $this->_scripts[$value];
            if ($this->_scriptsNeedPrefix[$value]) {
                $src = $this->_scriptsFolderPrefix . $src;
            }
            if ($this->_scriptsExt[$value] === null) {
                $src .= '.js';
            } elseif ($this->_scriptsExt[$value] != '') {
                $src .= '.' . $this->_scriptsExt[$value];
            }
            $html .= '<script type="text/javascript" src="' . $src . '"></script>'
            		 . "\n";
        }
        
        // styles
        $keys = $this->_getStylesSortedKeys();
        foreach ($keys as $value) {
            $src = $this->_styles[$value];
            if ($this->_stylesNeedPrefix[$value]) {
                $src = $this->_stylesFolderPrefix . $src;
            }
            if ($this->_stylesExt[$value] === null) {
                $src .= '.css';
            } elseif ($this->_stylesExt[$value] != '') {
                $src .= '.' . $this->_stylesExt[$value];
            }
            $html .= '<link rel="stylesheet" type="text/css" href="' . $src . '"/>'
            		 . "\n";
        }
        
        return $html;
    }
    
    /**
     * returns keys of scripts arrays sorted by prio
     * 
     * @return array
     */
    protected function _getScriptsSortedKeys()
    {
        return $this->_getSortedKeys($this->_scriptsPrio);
    }
    
    /**
     * returns keys of styles arrays sorted by prio
     * 
     * @return array
     */
    protected function _getStylesSortedKeys()
    {
        return $this->_getSortedKeys($this->_stylesPrio);
    }
    
    /**
     * sort givven array and returns it
     * 
     * @param array $temp
     * @return array
     */
    private function _getSortedKeys(array $temp)
    {
        $keys = array();
        while (count($temp) > 0) {
            $max = max($temp);
            foreach ($temp as $key => $value) {
                if ($value == $max) {
                    $keys[] = $key;
                    unset($temp[$key]);
                }
            }
        }
        return $keys;
    }
}