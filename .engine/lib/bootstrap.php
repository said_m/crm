<?php
date_default_timezone_set('Europe/Moscow');
define('SMARTY_DIR', PREFIX . '.engine/lib/smarty/');
//------------------------------------------------------------------------------------------------
// autoloading functionality
spl_autoload_register(null, false);
spl_autoload_extensions('.class.php');

set_include_path(get_include_path() . PATH_SEPARATOR . PREFIX . '.engine/lib/PEAR');

function classLoader($class)
{
    $dirs = array(realpath(__DIR__),
                  realpath(__DIR__ . '/PEAR'));
                  
	$needDir = true;
	if (strpos($class, '_') !== false) {
        $tmp = explode('_', $class);
        $len = sizeof($tmp);
        if ($tmp[0] != 'Zend') {
            $filename = $tmp[$len - 1] . '.class.php';
        } else {
            $filename = $tmp[$len - 1] . '.php';
            $needDir = false;
        }
        //$file = PREFIX . '.engine/lib/' . implode('/', array_slice($tmp, 0, $len - 1)) . '/' . $filename;
        $file = implode('/', array_slice($tmp, 0, $len - 1)) . '/' . $filename;
    } else {
        $filename = $class . '.class.php';
        //$file = PREFIX . '.engine/lib/' . $filename;
        $file = $filename;
    }
	//Logger::getInstance()->debug($file);
    $isFound = false;
    if ($needDir) {
        foreach ($dirs as $dir) {
            if (file_exists($dir . '/' . $file)) {
                $file = $dir . '/' . $file;
                $isFound = true;
                break;
            }
        }
    } else {
        //$isFound = file_exists($file);
        $isFound = true; // file_exists does not use include_path
    }

    if (!$isFound) {
        return false;
    }
    if(!isset($_SERVER['HTTP_HOST']) && isset($_SERVER['REMOTE_ADDR'])){
        $_SERVER['HTTP_HOST']=$_SERVER['REMOTE_ADDR'];
    }
    include $file;
}

spl_autoload_register('classLoader');

//--------------------------------------------------------------------------------------
// replacing default errors handler with exception
function err2exception($errno, $errstr, $errfile, $errline)
{
    if (!(error_reporting() & $errno)) {
        // code is not included in error_reporting
        return;
    }
    //var_dump($errstr);exit;
    throw new SiteException($errstr, $errno, $errfile, $errline);
}

set_error_handler('err2exception', E_ALL & ~E_NOTICE);
//--------------------------------------------------------------------------------------
$Args = array();
try
{
    $Args['LocalConfig'] = Config::getInstance();
} catch (SiteException $Exc)
{
    Logger::getInstance()->error('Error loading configuration file');
    Misc::basicError(_('Error loading configuration file'));
    exit;
}
//--------------------------------------------------------------------------------------------------------------
list($Args['path'], $Args['basePath']) = Misc::path($_SERVER['REQUEST_URI']);
$Args['reverse_path'] = array_reverse($Args['path']);
$Args['host_exploded'] = explode('.', $_SERVER['HTTP_HOST']);
$Args['QUERY_STRING'] = $_SERVER['QUERY_STRING'];
//--------------------------------------------------------------------------------------------------------------
if (isset($_REQUEST['sessionID'])) session_id($_REQUEST['sessionID']);
session_start();



?>