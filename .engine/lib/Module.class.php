<?php
class Module extends Mapper
 {
    const DBTABLE = 'erp_modules'; 
	protected static $table = 'erp_modules';

    function __construct($Params = array(), $init = 0)  
     {
      parent::__construct($Params, $init);
      
     }
 //--------------------------------------------------------------------------------
  	function getPermission($userID = '')
	 {
        try {
            $Connection = DBConnection::getInstance();
        } catch (SiteException $Exc) {
            return 0;
        }
        if ($SQLRES = $Connection->DBLink->query("SELECT perm FROM erp_user2module WHERE moduleID=$this->id AND userID=$userID")) 
        {
         $row = $SQLRES->fetch_row();
		 return $row[0];
        }  
		return 0;
	 }  
 //--------------------------------------------------------------------------------
  public function getUsers()
   {
        try {
            $Connection = DBConnection::getInstance();
        } catch (SiteException $Exc) {
            return array();
        }
     $Res = array();
	 try 
         {
           if ($SQLRES = $Connection->DBLink->query('SELECT erp_users.user_fname, erp_users.user_lname, erp_users.user_ogin, erp_users.user_position, erp_users.role_id, erp_users.id, erp_user2module.perm FROM erp_users, erp_user2module WHERE erp_user2module.moduleID=' . $this->id . ' AND erp_users.id=erp_user2module.userID ORDER BY erp_user2module.perm DESC')) 
            {
              while ($row = $SQLRES->fetch_assoc()) array_push($Res, new User($row, 1));
            }   
         }catch (SiteException $Esc) {}
      return $Res; 
   }
//--------------------------------------------------------------------------------
public static function get($Params = array())
{
	return parent::get($Params);
} 
//------------------------------------------------------------------------------------------------------------     
public static function count($Params = array())
{
	return parent::count($Params);
}
//------------------------------------------------------------------------------------------------------------   
public static function delete($Params = array())
{
	return parent::delete($Params);
}
//-------------------------------------------------------------------------------------
function __get($var) {
	switch ($var) {
		case 'Menu':
			$res = array();
			switch ($this->id) {
				case '2':
					return  array(
						array('title'=>'Компании', 'url'=>'/mcrm/companies/index.html'),
						array('title'=>'Задачи', 'url'=>'/mcrm/tasks/index.html'),
						array('title'=>'Календарь', 'url'=>'/mcrm/tasks/calendar/index.html'),
						array('title'=>'Сообщения', 'url'=>'/mcrm/messages/index.html')
						);
				break;
				case '6':
					return  array(
						array('title'=>'CRM : Группы контактов', 'url'=>'/mcategories/contact_groups/index.html'),
						array('title'=>'CRM : Типы задач', 'url'=>'/mcategories/task_categories/index.html'),
						array('title'=>'Персонал', 'url'=>'/mcategories/users/index.html')
						);
				break;
			}
			return $res;
		break;
	}
}

//-------------------------------------------------------------------------------------------------------
 }
?>