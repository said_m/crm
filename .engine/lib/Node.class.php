<?php

class Node extends Mapper
 { 
   protected static $db_table;
   function __construct($Params = array(), $init = 0)  
     {
		try {
			$Connection = DBConnection::getInstance();
		} catch (SiteException $Exc) {
			return false;
		}
	  if ($init == 1 && isset($Params)) foreach ($Params as $k=>$v) $this->$k = $v;
        else
           {
			$sqlParts = array();
			foreach ($Params as $k=>$v) array_push($sqlParts, $k . "='" . $v . "'");
			if ($SQLRES = $Connection->DBLink->query('SELECT * FROM ' . static::$table . ' WHERE ' . implode(' AND ', $sqlParts)))
			{
				if ($SQLRES->num_rows > 1) {$SQLRES->free();throw new SiteException(SiteException::DATABASE_QUERY_FAILED, 'Database query failed ['.$SQL.'];', __FILE__, __LINE__);}
				elseif($SQLRES->num_rows == 1) {$row = $SQLRES->fetch_assoc();foreach ($row as $k=>$v) $this->$k = $v;}
			}else throw new SiteException(SiteException::FAKE_OBJECT_INIT, 'Trying to init fake object', __FILE__, __LINE__);
           }
     }
  //-------------------------------------------------------------------------------
   function getSubTree($Params = array())
    {
		try {
			$Connection = DBConnection::getInstance();
		} catch (SiteException $Exc) {
			return false;
		}
		$class = get_class($this);
		$Res = array();
		$strWHERE = " WHERE lft > " . $this->lft . " AND rgt < " . $this->rgt;
		
		if (isset($Params['where']) && is_array($Params['where']) && count($Params['where'])) 
		{
	        foreach ($Params['where'] as $cond=>$val)
	         {
	           $strWHERE .= ' AND ';
			   if (is_array($val) && count($val) > 0) $strWHERE .= $cond .' IN(' . implode(',', array_map("escapeParam", $val)) . ')';
	           elseif ($val === false) $strWHERE .= $cond . ' IS NULL';
	           else $strWHERE .= $cond . '=' . $Connection->DBLink->qstr($val);
	         }
		}
		try {
			if ($SQLRES = $Connection->DBLink->query('SELECT * FROM ' . static::$table .  $strWHERE . ' ORDER BY lft')) 
			{
            	while ($row = $SQLRES->fetch_assoc()) array_push($Res, new $class($row, 1));
           	}   
       	}catch (SiteException $Esc) {}    
		return $Res;
    }  
  //-------------------------------------------------------------------------------
   function getSubTreeRaw()
    {
		try {
			$Connection = DBConnection::getInstance();
		} catch (SiteException $Exc) {
			return false;
		}
		$class = get_class($this);
		$Res = array();
		
		$strWHERE = " WHERE lft >= " . $this->lft . " AND rgt <= " . $this->rgt;
		
		if (isset($Params['where']) && is_array($Params['where']) && count($Params['where'])) 
		{
	        foreach ($Params['where'] as $cond=>$val)
	         {
	           $strWHERE .= ' AND ';
			   if (is_array($val) && count($val) > 0) $strWHERE .= $cond .' IN(' . implode(',', array_map("escapeParam", $val)) . ')';
	           elseif ($val === false) $strWHERE .= $cond . ' IS NULL';
	           else $strWHERE .= $cond . '=' . $Connection->DBLink->qstr($val);
	         }
		}
		try {
			if ($SQLRES = $Connection->DBLink->query('SELECT id FROM ' . static::$table .  $strWHERE . ' ORDER BY lft'))  
			{
            	while ($row = $SQLRES->fetch_row()) array_push($Res, $row[0]);
           	}   
       	}catch (SiteException $Esc) {}    
		return $Res;
    }  
//-------------------------------------------------------------------------------
   function getChildren($Params = array())
    {
		try {
			$Connection = DBConnection::getInstance();
		} catch (SiteException $Exc) {
			return false;
		}
     $class = get_class($this);
     $Res = array();
     $sqlParts = '';
     if (isset($Params['limit'])) $sqlParts .= ' LIMIT ' . $Params['limit'];
     try 
       {
         if ($SQLRES = $Connection->DBLink->query('SELECT * FROM ' . static::$table . ' WHERE parent=' . $this->id . ' ORDER BY lft' . $sqlParts))   
         {
            while ($row = $SQLRES->fetch_assoc()) array_push($Res, new $class($row, 1));
         }   
       }catch (SiteException $Esc) {}    
      return $Res;
    }  
//-------------------------------------------------------------------------------
   function getChildrenRaw($Params = array())
    {
		try {
			$Connection = DBConnection::getInstance();
		} catch (SiteException $Exc) {
			return false;
		}
     $class = get_class($this);
     $Res = array();
     $sqlParts = '';
     if (isset($Params['limit'])) $sqlParts .= ' LIMIT ' . $Params['limit'];
     try 
       {
         if ($SQLRES = $Connection->DBLink->query('SELECT id FROM ' . static::$table . ' WHERE parent=' . $this->id . ' ORDER BY lft' . $sqlParts))    
         {
            while ($row = $SQLRES->fetch_row()) array_push($Res, $row[0]);
         }   
       }catch (SiteException $Esc) {}    
      return $Res;
    }  
//-------------------------------------------------------------------------------
   function getOnlyChildren($Params = array())
    {
		try {
			$Connection = DBConnection::getInstance();
		} catch (SiteException $Exc) {
			return false;
		}
     $class = get_class($this);
     $Res = array();
     $sqlParts = '';
     if (isset($Params['limit'])) $sqlParts .= ' LIMIT ' . $Params['limit'];
	 try 
       {
         if ($SQLRES = $Connection->DBLink->query('SELECT * FROM ' . static::$table . ' WHERE (rgt-lft)=1 ORDER BY title' . $sqlParts))  
         {
            while ($row = $SQLRES->fetch_assoc()) array_push($Res, new $class($row, 1));
         }   
       }catch (SiteException $Esc) {}    
      return $Res;
    }  
//------------------------------------------------------------------------------- 
   function getParentBranch()
    {
		try {
			$Connection = DBConnection::getInstance();
		} catch (SiteException $Exc) {
			return false;
		}
     $class = get_class($this);
     $Res = array();
     $SQL = 'SELECT * FROM ' . static::$table . ' WHERE lft <= ' . $this->lft . ' AND rgt >= ' . $this->rgt . ' ORDER BY lft';
     try 
       {
         if ($SQLRES = $Connection->DBLink->query('SELECT * FROM ' . static::$table . ' WHERE (rgt-lft)=1 ORDER BY title' . $sqlParts)) 
         {
            while ($row = $SQLRES->fetch_assoc()) array_push($Res, new $class($row, 1));
         }   
       }catch (SiteException $Esc) {}    
      return $Res;
    }  
  //-------------------------------------------------------------------------------
   function getMaxLevel()
    {
		try {
			$Connection = DBConnection::getInstance();
		} catch (SiteException $Exc) {
			return false;
		}
		$class = get_class($this);
		$res = 0;
		try {
			if ($SQLRES = $Connection->DBLink->query('SELECT MAX(level) AS res FROM ' . static::$table)) 
			{
            	list($res) = $SQLRES->fetch_row();
           	}   
       	} catch (SiteException $Esc) {}    
      
		return $res;
    }  
  //-------------------------------------------------------------------------------
   function addChild($Params = array()) 
    {
		try {
			$Connection = DBConnection::getInstance();
		} catch (SiteException $Exc) {
			return false;
		}
     $class = get_class($this);
     $Connection->DBLink->query('LOCK TABLES `' . static::$table . '` WRITE'); 
     $Connection->DBLink->autocommit(FALSE);
     if ($Connection->DBLink->query('UPDATE ' . static::$table . ' SET rgt = rgt + 2,  lft = IF(lft > ' . $this->rgt . ', lft + 2, lft) WHERE rgt >= ' . $this->rgt))
       {
        $sqlParts = '';
        foreach ($Params as $k=>$v) $sqlParts .= ',' . $k . '=' . $Connection->DBLink->qstr($v);
        if ($Connection->DBLink->query('INSERT INTO ' . static::$table . ' SET lft = ' . $this->rgt . ', rgt = ' . $this->rgt . ' + 1, level = ' . $this->level . ' + 1, parent = ' . $this->id . $sqlParts)) 
          { 
            $retID = $Connection->DBLink->insert_id;
            $Connection->DBLink->commit();
            $Connection->DBLink->query('UNLOCK TABLES');
            return new $class(array('id'=>$retID));
          }                                    
        else {$Connection->DBLink->rollback();$Connection->DBLink->query('UNLOCK TABLES');return false;}
       }else{$Connection->DBLink->rollback();$Connection->DBLink->query('UNLOCK TABLES'); return false;}
    }
//------------------------------------------------------------------------------- 
  function deleteChildren($Params = array())
   {
		try {
			$Connection = DBConnection::getInstance();
		} catch (SiteException $Exc) {
			return false;
		}
       $class = get_class($this);
	   $Connection->DBLink->query('LOCK TABLES `' . static::$table . '` WRITE');	
       foreach ($Params as $nid)
        {
           $Node = new $class(array('id'=>$nid));
		   $Connection->DBLink->autocommit(FALSE);
           if ($Connection->DBLink->query('DELETE FROM ' . static::$table . ' WHERE lft >= ' . $Node->lft . ' AND rgt <= ' . $Node->rgt))
            {
             if (!$Connection->DBLink->query('UPDATE ' . static::$table . ' SET rgt=rgt-(' . $Node->rgt . '-' . $Node->lft . '+1), lft = IF(lft > ' . $Node->lft . ', lft - (' . $Node->rgt . '-' . $Node->lft . '+1), lft) WHERE rgt > ' . $Node->rgt)) {$Connection->DBLink->rollback();$ok = $Connection->DBLink->query('UNLOCK TABLES');return false;}    
            }else{$Connection->DBLink->rollback();$ok = $Connection->DBLink->query('UNLOCK TABLES');return false;}
          $Connection->DBLink->commit(); 
		       
        }
     $ok = $Connection->DBLink->query('UNLOCK TABLES');   
     return true; 
   }

//------------------------------------------------------------------------------- 
  function edit($Params = array())
   {
		try {
			$Connection = DBConnection::getInstance();
		} catch (SiteException $Exc) {
			return false;
		}
    $sqlParts = array();
    foreach ($Params as $k=>$v) array_push($sqlParts,  $k . "=" . $Connection->DBLink->qstr($v));
	if ($Connection->DBLink->query('UPDATE ' . static::$table . ' SET ' . implode(',', $sqlParts)  . ' WHERE id=' . $this->id)) return true;
    else return false;
   }
//------------------------------------------------------------------------------- 
  function moveNodeUP()
   {
		try {
			$Connection = DBConnection::getInstance();
		} catch (SiteException $Exc) {
			return false;
		}
     $class = get_class($this);
         if ($SQLRES = $Connection->DBLink->query('SELECT * FROM ' . static::$table  . ' WHERE rgt=' . $this->lft . '-1'))
         {
         	$row = $SQLRES->fetch_assoc();
			try {$UpNode = new $class($row, 1);}
			catch(SiteException $Exc) {}
		 }
         if ($UpNode->id > 0)
          {
           $DeltaLeft = $this->lft - $UpNode->lft;
           $CurrIds = array();
           if ($SQLRES = $Connection->DBLink->query('SELECT id FROM ' . static::$table .  ' WHERE lft >= ' . $this->lft . ' AND rgt <= ' . $this->rgt))
           {
           		while ($row = $SQLRES->fetch_row()) array_push($CurrIds, $row[0]);
		   }
           //---------------------------------------------------------------------------------
           $UpperIds = array();
           if ($SQLRES = $Connection->DBLink->query('SELECT id FROM ' . static::$table .  ' WHERE lft >= ' . $UpNode->lft . ' AND rgt <= ' . $UpNode->rgt))
           {
           		while ($row = $SQLRES->fetch_row()) array_push($UpperIds, $row[0]);
		   }
           $newRgtKey = $this->lft - $DeltaLeft +($this->rgt - $this->lft);
           
           $Connection->DBLink->autocommit(FALSE);
           if (!$Connection->DBLink->query("UPDATE " . static::$table . " SET lft=lft-$DeltaLeft, rgt=lft-$DeltaLeft+(rgt-lft) WHERE id IN(" . implode(',', $CurrIds) . ")")){$Connection->DBLink->rollback();return false;}    
            
           $downDeltaLeft = $newRgtKey + 1 - $UpNode->lft;
           if (!$Connection->DBLink->query("UPDATE " . static::$table . " SET lft=lft+$downDeltaLeft, rgt=lft+$downDeltaLeft+(rgt-lft) WHERE id IN(" . implode(',', $UpperIds) . ")")) {$Connection->DBLink->rollback();return false;}
           $Connection->DBLink->commit();
           return true;
          }else return false;
   }
//------------------------------------------------------------------------------- 
  function moveNodeDown()
   {
		try {
			$Connection = DBConnection::getInstance();
		} catch (SiteException $Exc) {
			return false;
		}
    $class = get_class($this);
    if ($SQLRES = $Connection->DBLink->query('SELECT * FROM ' . static::$table  . ' WHERE lft=' . $this->rgt . '+1')) 
	{
		$row = $SQLRES->fetch_assoc();
		try {$DownNode = new $class($row, 1);}
		catch(SiteException $Exc) {}
	}
    if ($DownNode->id > 0)
     {
           $DeltaLeft = $DownNode->lft - $this->lft;
           $CurrIds = array();
           if ($SQLRES = $Connection->DBLink->query('SELECT id FROM ' . static::$table .  ' WHERE lft >= ' . $DownNode->lft . ' AND rgt <= ' . $DownNode->rgt))
           {
           		while ($row = $SQLRES->fetch_row()) array_push($CurrIds, $row[0]);
		   }
           //---------------------------------------------------------------------------------
           $UpperIds = array();
           if ($SQLRES = $Connection->DBLink->query('SELECT id FROM ' . static::$table .  ' WHERE lft >= ' . $this->lft . ' AND rgt <= ' . $this->rgt))
           {
           		while ($row = $SQLRES->fetch_row()) array_push($UpperIds, $row[0]);
		   }
           $newRgtKey = $DownNode->lft - $DeltaLeft +($DownNode->rgt - $DownNode->lft);
           $Connection->DBLink->autocommit(FALSE);
           if (!$Connection->DBLink->query("UPDATE " . static::$table . " SET lft=lft-$DeltaLeft, rgt=lft-$DeltaLeft+(rgt-lft) WHERE id IN(" . implode(',', $CurrIds) . ")")){$Connection->DBLink->rollback();return false;}    
           $downDeltaLeft = $newRgtKey + 1 - $this->lft;
           if (!$Connection->DBLink->query("UPDATE " . static::$table . " SET lft=lft+$downDeltaLeft, rgt=lft+$downDeltaLeft+(rgt-lft) WHERE id IN(" . implode(',', $UpperIds) . ")")) {$Connection->DBLink->rollback();return false;}
           $Connection->DBLink->commit();
           return true;
     }else return false;
   }
  //-----------------------------------------------------------------------------------
 } 
?>