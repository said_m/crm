<?php
class User extends Mapper
 {
    const DBTABLE = 'erp_users'; 
	protected static $table = 'erp_users';
	public $class_title;
    function __construct($Params = array(), $init = 0, $create = 0)  
     {
      
	  	if ($create == 1) $this->create($Params);
      	else parent::__construct($Params, $init);
		
        $this->class_title = $this->user_ogin;  
	 }
   function __destruct()
    {
     
    }
	 
//------------------------------------------------------------------------------- 
public static function get($Params = array())
{
	return parent::get($Params);
} 
//------------------------------------------------------------------------------------------------------------     
public static function count($Params = array())
{
	return parent::count($Params);
}
//------------------------------------------------------------------------------------------------------------   
public static function delete($Params = array())
{
	return parent::delete($Params);
}
//-------------------------------------------------------------------------------------
function __get($var) {
	switch ($var) {
		case 'newCRMMessagesCnt':
			return CRM_Message::count(array('where'=>array('touserID'=>$this->id,'deleted'=>'0', 'viewed'=>'0')));
		break;
		case 'newCRMTasksCnt':
			return 0;
		break;
	}
}

//-------------------------------------------------------------------------------------------------------
 }
?>