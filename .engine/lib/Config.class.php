<?php
class Config
{
	static private $instance_auth = NULL;
	static private $instance = NULL;
	private $confFile;
	private $conf;
	static function getInstance($conf_file = '')
    {
        if ($conf_file == '') {
			if (self::$instance_auth == NULL) self::$instance_auth = new Config();
			return self::$instance_auth;
		} else {
			if (self::$instance == NULL) self::$instance = new Config($conf_file);
			return self::$instance;
		}
    }
    private function __construct($conf_file = '')
    {
  		if (!strlen($conf_file)) $conf_file = PREFIX . '.engine/etc/main.conf';
		$this->confFile = $conf_file;
  		if (!is_file($this->confFile)) {throw new SiteException($this->confFile . ' not found');return;}
		$this->conf = $this->arr2obj(parse_ini_file($this->confFile, true));
	}
	private function __clone(){}
//--------------------------------------------------------------------------------------------
function getConf()
{
	return $this->conf;
}
//--------------------------------------------------------------------------------------------
private function arr2obj($arr)
{
	$Res = new stdClass();
	if (is_array($arr) && sizeof($arr))
	{
		foreach ($arr as $k=>$v)
		{
			if (is_array($v)) $Res->$k = (object) $v;
			else $Res->$k = $v;
		}
	}
	return $Res;
}
//--------------------------------------------------------------------------------------------

static function save($Params = array())
{
	$ini_str = ''; $errors = array();
	if (count($Params)) {
		foreach ($Params as $name=>$section) {
			$ini_str .= '['.$name."]\n";
			$check = Config::checkSection($name, $section);
			
			if ($check[0]) {
				if (!isset($section['title'])) $ini_str .= "title=".$name."\n";
				
				foreach ($section as $k=>$v) {
					$ini_str .= $k."=".htmlspecialchars(trim($v))."\n";
				}
			} else $errors[] = $check[1];
		}
		
		if (count($errors) == 0) {
			if ($handle = fopen(PREFIX . '.engine/etc/main.conf', 'w')) {
				if (fwrite($handle, $ini_str) === FALSE) {
					return array(false, _("Can't write to INI file!"));
				}
				fclose($handle);
			}
			return array(true, '');
		} else return array(false, implode('<br />', $errors));
	}
}
//--------------------------------------------------------------------------------------------
static function checkSection($name = '', $data = array())
{
	switch ($name) {
		case 'database':
			try {
				$DBLink = pg_connect("host=" . $data['host_read'] . " port=5432 dbname=" . $data['dbname'] . " user=" . $data['login'] . " password=" . $data['pass']);
			} catch (SiteException $Exc){
				return array(false, 'Read Database connection failed');
			}
			try {
				$DBLink = pg_connect("host=" . $data['host_write'] . " port=5432 dbname=" . $data['dbname'] . " user=" . $data['login'] . " password=" . $data['pass']);
			} catch (SiteException $Exc){
				return array(false, 'Read Database connection failed');
			}
		break;
		case 'db_stat':
			try {
				$DBLink = pg_connect("host=" . $data['host'] . " port=5432 dbname=" . $data['dbname'] . " user=" . $data['dblogin'] . " password=" . $data['dbpass']);
			} catch (SiteException $Exc){
				return array(false, 'Stat DB connection failed');
			}
		break;
		/*
		case 'encoding':
			$Headers = @get_headers($data['video']);
			if(!preg_match("|200|", $Headers[0])) return array(false, 'Encoding video file failed');
			$Headers = @get_headers($data['image']);
			if(!preg_match("|200|", $Headers[0])) return array(false, 'Encoding image file failed');
			$Headers = @get_headers($data['videostatus']);
			if(!preg_match("|200|", $Headers[0])) return array(false, 'Encoding videostatus file failed');
			$Headers = @get_headers($data['imagestatus']);
			if(!preg_match("|200|", $Headers[0])) return array(false, 'Encoding imagestatus file failed');
		
		break;
		*/
		case 'cloud':

		break;
	}
	
	return array(true,'');
}

//--------------------------------------------------------------------------------------------
}	
?>