<?php

class AuthMapper {

//-----------------------------------------------------------------------------------------------------------------------------
    protected static $table;
	protected $fake;
    function __construct($Params = array(), $init = 0) {
        if ($init == 1 && count($Params) > 0) {
            foreach ($Params as $k => $v)
                $this->$k = $v;
        } else {
            $Connection = DBConnectionAuth::getInstance();

            $sqlParts = array();
            if (count($Params) > 0) {
                foreach ($Params as $k => $v)
                    array_push($sqlParts, $k . "='" . $v . "'");
                $SQL = "SELECT * FROM " . static::$table . " WHERE " . implode(' AND ', $sqlParts);
            } else
                $SQL = "SELECT * FROM " . static::$table . " LIMIT 1";


			if ($SQLRES = $Connection->DBLink->query($SQL))
            {
				if ($SQLRES->num_rows > 1) throw new SiteException(SiteException::DATABASE_QUERY_FAILED, 'Database query failed ['.$SQL.'];', __FILE__, __LINE__);
				elseif($SQLRES->num_rows == 1) {$row = $SQLRES->fetch_assoc();foreach ($row as $k=>$v) $this->$k = $v;}
				else $this->fake = true;
				$SQLRES->free();
			}else $this->fake = true;

			if ($this->fake === true) throw new SiteException('Trying to init fake object ' . $SQL);
        }
    }

//-------------------------------------------------------------------------------------------------------------

    public static function restrictions($Params = array()) {
		$Res = array('where' => '', 'order' => '', 'limit' => '', 'join' => '', 'fields'=>'*');
        try {
            $Connection = DBConnectionAuth::getInstance();
        } catch (SiteException $Exc) {
            return array();
        }
        $className = get_called_class();
        //--------------------------------------------------------------------------------------
        if (method_exists($className, 'getAccessRestrictions')) {
            $restrictions = $className::getAccessRestrictions();
            foreach ($restrictions as $k => $v) {
                $Params['where'][$k] = $v;
            }
        }
        //--------------------------------------------------------------------------------------
        if (isset($Params['where']) && is_array($Params['where']) && count($Params['where'])) {
            $Res['where'] .= ' WHERE ';
            $i = 0;
            foreach ($Params['where'] as $cond => $val) {
                if ($i > 0)
                    $Res['where'] .= ' AND ';
                $i++;
                if ($cond == 'append') {
                    $Res['where'] .= " " . $Params['where']['append'];
                    continue;
                }
                if (is_array($val) && count($val) > 0)
                    $Res['where'] .= $cond . ' IN(' . implode(',', array_map("$Connection->DBLink->qstr", $val)) . ')';
                elseif ($val === false)
                    $Res['where'] .= $cond . ' IS NULL';
                else
                    $Res['where'] .= $cond . '=' . $Connection->DBLink->qstr($val) . '';
            }
        }

		if (isset($Params['fields']) && is_array($Params['fields']) && count($Params['fields']) > 0)
		 	$Res['fields'] = implode(',', $Params['fields']);

        if (isset($Params['order']) && strlen($Params['order']))
            $Res['order'] = ' ORDER BY ' . $Params['order'];


        if (isset($Params['limit']) && strlen($Params['limit'])) {
            $Res['limit'] = " LIMIT " . $Params['limit'];
            if (isset($Params['offset'])) {
                $Res['limit'] .= " OFFSET " . $Params['offset'];
            }
        }
        /**
         *  Use like Item::get(array('join'=>array('cms_playlist2item'=>'cms_playlist2item.item_id=cms_item.id')));         
         */
        if (isset($Params['join']) && is_array($Params['join'])) {
            foreach ($Params['join'] as $join_table => $join_condition) {
                $Res['join'] .= " LEFT JOIN $join_table ON ($join_condition) ";
            }
        }

        return $Res;
    }

//-------------------------------------------------------------------------------------------------------------
    public static function get($Params = array()) {
        try {
            $Connection = DBConnectionAuth::getInstance();
        } catch (SiteException $Exc) {
            return array();
        }
        $Res = array();
        $className = get_called_class();
        $Restrictions = self::restrictions($Params);
		
		$SQL = "SELECT " . $Restrictions['fields'] . " FROM " . static::$table . " " . $Restrictions['join'] . $Restrictions['where'] . $Restrictions['order'] . $Restrictions['limit'];
        //Logger::getInstance()->debug($SQL);
        try {
            $SQLRES = $Connection->DBLink->query($SQL);
            if ($SQLRES) {
                while ($row = $SQLRES->fetch_assoc()) {
                    $Res[] = new $className($row, 1);
                }
            }
        } catch (SiteException $Exc) {
            Logger::getInstance()->error($Exc->getMessage());
        }
        return $Res;
    }
//-------------------------------------------------------------------------------------------------------------
    public static function getOne($Params = array()) {
        $className = get_called_class();
        $Res = $className::get($Params);
        if (count($Res))
            return $Res[0];
        return false;
    }
//-------------------------------------------------------------------------------------------------------------
    public static function count($Params = array()) {
        try {
            $Connection = DBConnectionAuth::getInstance();
        } catch (SiteException $Exc) {
            return array();
        }
        $className = get_called_class();
        $Restrictions = self::restrictions($Params);
        $SQL = "SELECT COUNT(*) FROM " . static::$table . " " . $Restrictions['join'] . $Restrictions['where'] . $Restrictions['order'] . $Restrictions['limit'];
        //Logger::getInstance()->debug($SQL);
        try {
            $SQLRES = $Connection->DBLink->query($SQL);
            if ($SQLRES) {
                list($Res) = $SQLRES->fetch_row();
            }
        } catch (SiteException $Esc) {
            
        }


        return $Res;
    }

//-------------------------------------------------------------------------------------------------------------
    function edit($Params = array()) {
		try {
            $Connection = DBConnectionAuth::getInstance();
        } catch (SiteException $Exc) {
            return false;
        }

        if (is_array($Params) && count($Params) > 0) {
            $SqlParts = '';
            $keys = array_keys($Params);
            $vals = array_values($Params);
            $cnt = count($keys);
            for ($i = 0; $i < $cnt; $i++) {
                if ($vals[$i] === false)
                    $SqlParts .= $keys[$i] . "=NULL"; else
                    $SqlParts .= $keys[$i] . "=" . $Connection->DBLink->escapeParam($vals[$i]);
                if ($i < $cnt - 1)
                    $SqlParts .= ', ';
            }
            $SQL = "UPDATE " . static::$table . " SET " . rtrim($SqlParts, ',') . " WHERE id=" . $this->id;
            //Logger::getInstance()->debug($SQL);
            $res = $Connection->DBLink->query($SQL);
            if ($res) {
                foreach ($Params as $k => $v)
                    $this->$k = $v; return true;
            }
        }
        return false;
    }

//-------------------------------------------------------------------------------------------------------------
    public function create($Params = array()) {
        try {
            $Connection = DBConnectionAuth::getInstance();
        } catch (SiteException $Exc) {
            return false;
        }

        $res = false;
        if (is_array($Params) && count($Params) > 0) {
            try {
				foreach ($Params as $k=>$v) {
					$keys[] = $k;
					$vals[] = $Connection->DBLink->escapeParam($v);
				}
                
				$SQL = 'INSERT INTO ' . static::$table . '( ' . implode(',', $keys) . ' ) VALUES(' . implode(',', $vals) . ')';
                
				$SQLRES = $Connection->DBLink->query($SQL);
				if ($SQLRES) {
					$retID = $Connection->DBLink->insert_id;
					if ($retID > 0) {
						foreach($Params as $k=>$v) $this->$k = $v;
						$this->id = $retID;
						return true;
					}
				}
				
            } catch (SiteException $Exc) {
				$res = false;
            }
        }
        return $res;
    }

//-------------------------------------------------------------------------------------------------------------
    public static function delete($del) {

        try {
            $Connection = DBConnectionAuth::getInstance();
        } catch (SiteException $Exc) {
            return false;
        }
        $del_array = array();
        if (is_object($del)) {
            $del_array[] = $del->id;
        } elseif (is_array($del)) {
            $del_array = $del;
        } else {
            $del_array[] = $del;
        }
        if (count($del)) {

            $SQL = 'DELETE FROM ' . static::$table . ' WHERE id IN(' . implode(',', $del_array) . ')';
            //Logger::getInstance()->debug($SQL);
            if ($Connection->DBLink->query($SQL))
                return true;
        }
        return false;
    }

//-------------------------------------------------------------------------------------------------------------
}

?>