<?php

class Logger {

    static private $instance = NULL;

    /**
     * 
     * @return Logger
     */
    static function getInstance() {
        if (self::$instance == NULL) {
            self::$instance = new Logger();
        }
        if (!isset($_SERVER['HTTP_HOST'])) {
            $_SERVER['HTTP_HOST'] = $_SERVER['REMOTE_ADDR'];
        }
        return self::$instance;
    }

    private function __construct() {

    }

    private function __clone() {
        
    }

//--------------------------------------------------------------------------------------------
    public function message($message) {
        if (is_array($message) || is_object($message))
            $message = print_r($message, true);
        $logString = '[' . date('d-m-Y H:i:s') . "]\t" . 'Message: ' . $message . ' URL: ' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'] . "\n";
        error_log($logString, 3, PREFIX . '.engine/var/log/message.log');
    }

//--------------------------------------------------------------------------------------------
    public function error($message) {
        if (is_array($message) || is_object($message))
            $message = print_r($message, true);
        $logString = '[' . date('d-m-Y H:i:s') . "]\t" . 'Error: ' . $message . ' URL: ' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'] . "\n";
        error_log($logString, 3, PREFIX . '.engine/var/log/error.log');
    }

//--------------------------------------------------------------------------------------------
    public function debug($message, $fileName = 'debug') {
        if (is_array($message) || is_object($message))
            $message = print_r($message, true);
        $logString = '[' . date('d-m-Y H:i:s') . "]\t" . 'Debug: ' . $message . ' URL: ' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'] . "\n";
        error_log($logString, 3, PREFIX . '.engine/var/log/' . $fileName . '.log');
    }

//--------------------------------------------------------------------------------------------
    public function exception(Exception $Exc) {
        $logString = '[' . date('d-m-Y H:i:s') . "]\t" . 'Exception: ' . $Exc->getMessage() . ' File: ' . $Exc->getFile() . ' Line: ' . $Exc->getLine() . ' URL: ' . $Exc->getUrl() . "\n";
        error_log($logString, 3, PREFIX . '.engine/var/log/error.log');
    }

//--------------------------------------------------------------------------------------------
    public function custom($message) {
        if (is_array($message) || is_object($message))
            $message = print_r($message, true);
        $logString = '[' . date('d-m-Y H:i:s') . "]\t" . 'Debug: ' . $message . ' URL: ' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'] . "\n";
        error_log($logString, 3, PREFIX . '.engine/var/log/custom.log');
    }

//--------------------------------------------------------------------------------------------
    public function sql($message) {
        $logString = '[' . date('d-m-Y H:i:s') . "]\t" . 'Debug: ' . $message . ' URL: ' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'] . "\n";
        error_log($logString, 3, PREFIX . '.engine/var/log/sql.log');
    }

//--------------------------------------------------------------------------------------------
}

?>