<?php

class Misc {

//--------------------------------------------------------------------------------------------------------------------------
    public static function basicError($message) {
        echo '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /><title>', $_SERVER['HTTP_HOST'], '</title></head><body><div style="width: 600px; margin: 50px 0 0 50px; border: 1px solid #999999;"><div style="background-color: #CC0000; color: #CC0000; padding:5px;">.</div><div style="font-size: 16px; padding: 8px;">', $message, '</div></div></body></html>';
    }

//--------------------------------------------------------------------------------------------------------------------------
    public static function path($uri) {
        $pos = strpos($uri, '?');
        if ($pos !== false) {
            $uri = substr(strtolower($uri), 0, $pos);
        }else
            $uri = strtolower($uri);
        $path = explode('/', str_replace('//', '/', $uri));
        $path = self::array_remove_empty($path);
        $basePath = (isset($path[0]) && ($path[0] == 'dev' || preg_match("/^v\d+?\.\d+?$/i", $path[0]))) ? '/' . $path[0] . '/' : '/';
        $path = self::array_remove_versioning($path);
        $path = array_map('rawurldecode', $path);
        return array($path, $basePath);
    }

//--------------------------------------------------------------------------------------------------------------------------
    public static function array_remove_empty($array = array()) {
        $cnt = count($array);
        $res = array();
        if ($cnt) {
            for ($i = 0; $i < $cnt; $i++) {
                if ($array[$i] != '' && $array[$i] != 'index.html')
                    $res[] = $array[$i];
            }
        }
        return $res;
    }

//--------------------------------------------------------------------------------------------------------------------------
    public static function array_remove_versioning($array = array()) {
        $cnt = count($array);
        $res = array();
        if ($cnt) {
            if ($array[0] == 'dev' || $array[0] == 'trunk' || preg_match("/^v\d+?\.\d+?$/i", $array[0])) {
                $res = array_slice($array, 1);
            }else
                return $array;
        }
        return $res;
    }

//--------------------------------------------------------------------------------------------------------------------------
    public static function post($vars = array(), $htmlspecialchars = false) {
        $post = array();
        if (!count($vars))
            return $post;
        if ($htmlspecialchars) {
            foreach ($vars as $k => $v)
                $post[$k] = htmlspecialchars(trim($v));
        } else {
            foreach ($vars as $k => $v)
                if (!is_array($v) && !is_object($v))
                    $post[$k] = trim($v);
                else
                    $post[$k] = $v;
        }

        return $post;
    }

//--------------------------------------------------------------------------------------------------------------------------
    public static function created() {
        return date("Y-m-d H:i:s");
    }

//--------------------------------------------------------------------------------------------------------------------------
    static public function getSequence($Params = array()) {

        $cnt = count($Params['path']);
        if (substr($Params['path'][$cnt - 1], 0, 5) == 'page-') {
            $tmp = explode('-', $Params['path'][$cnt - 1]);
            $Params['currIdx'] = $tmp[1];
            unset($Params['path'][$cnt - 1]);
        }

        $Res = new StdClass;
        $Res->current = $Params['currIdx'];
        $Res->limitString = $Params['currIdx'] * $Params['ItemsPerPage'] . ', ' . $Params['ItemsPerPage'];
        $pages = ceil($Params['allItems'] / $Params['ItemsPerPage']);
        if ($pages > 1) {
            $leftIdx = 0;
            $rightIdx = 0;

            if (($Params['currIdx'] - 4) <= 0) {
                $leftIdx = 0;
                $rightIdx = 6;
                if ($rightIdx > $pages - 1)
                    $rightIdx = $pages - 1;
            }else if (($Params['currIdx'] + 4) > $pages) {
                $leftIdx = $pages - 7;
                $rightIdx = $pages - 1;
            } else {
                $leftIdx = $Params['currIdx'] - 3;
                $rightIdx = $Params['currIdx'] + 3;
            }

            if ($Params['currIdx'] > 0)
                $Res->prev = '/' . implode('/', $Params['path']) . '/page-' . ($Params['currIdx'] - 1) . '/' . $Params['query_string'];
            if ($Params['currIdx'] < $pages - 1)
                $Res->next = '/' . implode('/', $Params['path']) . '/page-' . ($Params['currIdx'] + 1) . '/' . $Params['query_string'];
            $Res->pages = array();
            for ($i = $leftIdx; $i <= $rightIdx; $i++)
                $Res->pages["$i"] = '/' . implode('/', $Params['path']) . '/page-' . $i . '/' . $Params['query_string'];
        }
        return $Res;
    }

//--------------------------------------------------------------------------------------------------------------------------
    public static function sendMail($from = '', $from_title = '', $to = '', $subject = '', $body = '') {
        if (!strlen($from) || !strlen($from_title) || !strlen($to) || !strlen($subject) || !strlen($body))
            return;
        try {
            require_once 'Zend/Mail.php';
            require_once 'Zend/Mail/Transport/Smtp.php';
            
            //$config = array('name' => 'previewnetworks.com');
            $config = array();
            $smtp_server = (isset(Config::getInstance()->getConf()->misc->email_server)) ? Config::getInstance()->getConf()->misc->email_server : 'mail.my.filmtrailer.com';
            $transport = new Zend_Mail_Transport_Smtp($smtp_server, $config);

            Zend_Mail::setDefaultFrom($from, $from_title);

            $mail = new Zend_Mail('UTF-8');
            $mail->setType(Zend_Mime::MULTIPART_RELATED);

            $mail->addTo(trim($to), '');

            $mail->setSubject($subject);
            //Logger::getInstance()->debug($body);
            //----------------------------------------------------------------------------------------
            // parsing inline images
              $matches = array();	        
              preg_match_all("#<img.*?src=['\"]([^'\"]+)#ism", $body, $matches);
	      if (isset($matches[1])){
                  $matches = array_unique($matches[1]);
                  
              }

            //$dom = new DOMDocument(null, 'UTF-8');
            //@$dom->loadHTML($body);
            //$images = $dom->getElementsByTagName('img');
            $sources = array();
            $cnt = count($matches);
            if ($cnt) {
                for ($i = 0; $i < $cnt; $i++) {
                    //$img = $images->item($i);
                    // assuming that files are on local server and related to document root like src="images/0.gif"
                    //$src = $img->getAttribute('src');
                    $src = $matches[$i];
                    $file = str_replace('//', '/', PREFIX . $src);
                    if (is_file($file) && !isset($sources[$src])) {
                        $img_id = sha1(time() . $src);
                        $sources[$src] = 'cid:' . $img_id;
                        $pathinfo = pathinfo($file);


                        $mime = new Zend_Mime_Part(file_get_contents($file));
                        $mime->id = $img_id;
                        $mime->type = Misc::mimetype($src);
                        $mime->disposition = Zend_Mime::DISPOSITION_INLINE;
                        $mime->encoding = Zend_Mime::ENCODING_BASE64;
                        $mime->filename = $pathinfo['basename'];
                        
                        $mail->addAttachment($mime);
                    }
                }
            }
            
            if (count($sources)){
                foreach ($sources as $src=>$cid){
                    $body = str_replace('src="' . $src . '"', 'src="' . $cid . '"', $body);
                }
            }
            //----------------------------------------------------------------------------------------
            //Logger::getInstance()->debug($body);
            
            
            $mail->setBodyHtml($body,'UTF-8', Zend_Mime::ENCODING_8BIT);

            Logger::getInstance()->debug($mail->send($transport));

            

            return true;
        } catch (SiteException $Exc) {
            return false;
        }
    }

//--------------------------------------------------------------------------------------------------------------------------
    public static function stripN($str) {
        return str_replace("\n", '', str_replace("\r", '', trim($str)));
    }

//--------------------------------------------------------------------------------------------------------------------------
    public static function object2Array($d) {
        if (is_object($d)) {
            $d = get_object_vars($d);
        }

        if (is_array($d)) {
            return array_map(__FUNCTION__, $d);
        } else {
            return $d;
        }
    }

//--------------------------------------------------------------------------------------------------------------------------
    public static function trimList($val) {
        $val = trim($val);
        if (strlen($val) && $val[strlen($val) - 1] == ',') {
            $val = substr($val, 0, strlen($val) - 1);
        }
        return $val;
    }

//--------------------------------------------------------------------------------------------------------------------------
    public static function objects2ids($ObjArr = array()) {
        $Res = array();
        foreach ($ObjArr as $Obj) {
            $Res[] = $Obj->id;
        }
        return $Res;
    }

//-------------------------------------------------------------------------------------------------------    
    public function generatePassword() {
        $length = mt_rand(5, 10);
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!@#$&,.?';
        $string = '';

        for ($p = 0; $p < $length; $p++) {
            $string .= $characters[mt_rand(0, strlen($characters) - 1)];
        }

        return $string;
    }

//-------------------------------------------------------------------------------------------------------
}

?>