<?php
//-------------------------------------------------------------------------------------------------------
class CRM_Task extends Mapper
 {
	protected static $table = 'erp_crm_tasks';
	public $class_title;
    function __construct($Params = array(), $init = 0, $create = 0)  
     {
       if ($create == 1) $this->create($Params);
       else parent::__construct($Params, $init);
	   
	   $this->class_title = $this->title;  
     }
 //--------------------------------------------------------------------------------
    public function assignFiles($files = array())
    {
		try {
			$Connection = DBConnection::getInstance();
		} catch (SiteException $Exc) {
			return false;
		}
		if (count($files) > 0) {
			$Connection->DBLink->query('DELETE FROM erp_task2files WHERE taskID=' . $this->id);
			foreach ($files as $fileID) $Connection->DBLink->query("INSERT INTO erp_task2files(fileID, taskID) VALUES(" . $fileID . ", " . $this->id . ")");
		} 
    }
 //--------------------------------------------------------------------------------
   public function  unsetFiles()
    {
		try {
			$Connection = DBConnection::getInstance();
		} catch (SiteException $Exc) {
			return false;
		}
		$Connection->DBLink->query('DELETE FROM erp_task2files WHERE taskID=' . $this->id);
    }   
//--------------------------------------------------------------------------------
  public function getTaskFiles()
   {
		try {
			$Connection = DBConnection::getInstance();
		} catch (SiteException $Exc) {
			return false;
		}
		$Res = array();
		$SQL = 'SELECT erp_notefiles.* FROM erp_notefiles, erp_task2files WHERE erp_task2files.taskID=' . $this->id . ' AND erp_notefiles.id=erp_task2files.fileID ORDER BY erp_notefiles.file_name';
		try {
			if ($SQLRES = $Connection->DBLink->query($SQL)) {
				while ($row = $SQLRES->fetch_assoc()) array_push($Res, new NoteFile($row, 1));
			}   
		} catch (SiteException $Esc) {}
		return $Res; 
   }
//--------------------------------------------------------------------------------
public static function get($Params = array())
{
	if (isset($Params['filter']) && is_array($Params['filter'])) {
		$append = '';
		if ($Params['filter']['filter_fld'] == 'comp_name') {
			$append .= "contactID IN (SELECT id FROM erp_crm_contacts WHERE companyID IN ".
						"(SELECT id FROM erp_companies WHERE lcase(comp_name) LIKE '%" . $Params['filter']['filter_val'] . "%')) ";
		} else {
			$append .= "lcase(" . $Params['filter']['filter_fld'] . ") LIKE '%" . $Params['filter']['filter_val'] . "%'";
		}
		$Params['where']['append'] = (strlen($Params['where']['append'])) ? $Params['where']['append'] . ' AND ' . $append : $append;
	}
	return parent::get($Params);
} 
//------------------------------------------------------------------------------------------------------------     
public static function count($Params = array())
{
	if (isset($Params['filter']) && is_array($Params['filter'])) {
		$append = '';
		if ($Params['filter']['filter_fld'] == 'comp_name') {
			$append .= "contactID IN (SELECT id FROM erp_crm_contacts WHERE companyID IN ".
						"(SELECT id FROM erp_companies WHERE lcase(comp_name) LIKE '%" . $Params['filter']['filter_val'] . "%')) ";
		} else {
			$append .= "lcase(" . $Params['filter']['filter_fld'] . ") LIKE '%" . $Params['filter']['filter_val'] . "%'";
		}
		$Params['where']['append'] = (strlen($Params['where']['append'])) ? $Params['where']['append'] . ' AND ' . $append : $append;
	}
	return parent::count($Params);
}
//------------------------------------------------------------------------------------------------------------   
public static function delete($Params = array())
{
	return parent::delete($Params);
}
//-----------------------------------------------------------------------------------
public static function getTasksByHour($Params = array())
{
		try {
			$Connection = DBConnection::getInstance();
		} catch (SiteException $Exc) {
			return false;
		}
		$Res = array();
		for ($i=0;$i<24;$i++) $Res[$i] = array();
		
		if (!isset($Params['date'])) $Params['date'] = date('Y-m-d'); 
		list($Year,$Month,$Day) = explode('-',$Params['date']);

		try {
			if ($SQLRES = $Connection->DBLink->query('SELECT *,HOUR(expiry_time) AS hr FROM erp_crm_tasks WHERE userID=' . $Params['userID'] . " AND expiry_date='" . $Params['date'] . "' AND deleted='0' ORDER BY expiry_time")) {
				while ($row = $SQLRES->fetch_assoc()) {
					if (!is_array($Res[$row['hr']])) $row['hr'] = array();
					array_push($Res[$row['hr']], new CRM_Task($row,1));
				}
			}   
		} catch (SiteException $Esc) {}
	  	return $Res;
}  
//-------------------------------------------------------------------------------------
function __get($var) {
	switch ($var) {
		case 'FromUser':
			if (isset($this->createbyID) && $this->createbyID > 0) {
				try {
					return new User(array('id' => $this->createbyID));
				} catch (SiteException $Exc) {}
			}
			return false;
		break;
		case 'ToUser':
			if (isset($this->userID) && $this->userID > 0) {
				try {
					return new User(array('id' => $this->userID));
				} catch (SiteException $Exc) {}
			}
			return false;
		break;
		case 'TaskCategory':
			$res = array();
			if (isset($this->categoryID) && strlen($this->categoryID)) {
				try {
					return new CRM_TaskCategory(array('id' => $this->categoryID));
				} catch (SiteException $Exc) {}
			}
			return $res;
		break;
		case 'state_title':
			$res = array(
				'open'=>_('Открыт'),
				'done'=>_('Выполнен'),
				'not done'=>_('Не выполнен'),
				'attempted'=>_('Была попытка'),
				'received call'=>_('Получено требование'),
				'left message'=>_('Оставлено сообщение'),
				'held'=>_('Отслеживается'),
				'not held'=>_('Не отслеживается'),
				'postponed'=>_('Отложен'),
				'deleted'=>_('Удален')
				);
			return $res[$this->state];
		break;
		case 'priority_title':
			$res = array('high'=>_('Высокий'),'medium'=>_('Средний'),'low'=>_('Низкий'));
			return $res[$this->priority];
		break;
	}
}

//-------------------------------------------------------------------------------------------------------
 }
?>