<?php
//-------------------------------------------------------------------------------------------------------
class CRM_Message extends Mapper
{
    const DBTABLE = 'erp_crm_messages';
	protected static $table = 'erp_crm_messages';
    function __construct($Params = array(), $init = 0, $create = 0)  
     {
       if ($create == 1) $this->create($Params);
       else parent::__construct($Params, $init);
       //if (strlen($this->smsRecipients)) $this->smsRecipientsArray = unserialize($this->smsRecipients);
     }
//------------------------------------------------------------------------------- 
public static function get($Params = array())
{
	return parent::get($Params);
} 
//------------------------------------------------------------------------------------------------------------     
public static function count($Params = array())
{
	return parent::count($Params);
}
//------------------------------------------------------------------------------------------------------------   
public static function delete($Params = array())
{
	return parent::delete($Params);
}
//-------------------------------------------------------------------------------------
function __get($var) {
	switch ($var) {
		case 'FromUser':
			if (isset($this->userID) && $this->userID > 0) {
				try {
					return new User(array('id' => $this->userID));
				} catch (SiteException $Exc) {}
			}
			return false;
		break;
		case 'ToUser':
			if (isset($this->touserID) && $this->touserID > 0) {
				try {
					return new User(array('id' => $this->touserID));
				} catch (SiteException $Exc) {}
			}
			return false;
		break;
		case 'CCUsers':
			$res = array();
			if (isset($this->cc_users) && strlen($this->cc_users)) {
				$ids = explode(',', $this->cc_users);
				if (count($ids) > 0) {
					foreach ($ids as $uid) {
						try {
							$res[] = new User(array('id' => $uid));
						} catch (SiteException $Exc) {}
					}
				}
			}
			return $res;
		break;
	}
}

//-------------------------------------------------------------------------------------------------------
}
?>