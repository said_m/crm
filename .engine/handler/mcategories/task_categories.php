<?php   

if (isset($_POST) && count($_POST) > 0 && isset($_POST['action'])) {
	$responseJson['action'] = $_POST['action'];
    switch ($_POST['action']) {
        case 'add':
			$Args['post'] = Misc::post($_POST['enter']);
			
			if (!isset($Args['post']['title']) || !strlen($Args['post']['title']))
                $responseJson['errors']['form']['title'] = _('Поле "Название" заполнено некорректно');
				
			if (count($responseJson['errors']) == 0) {
				$Args['post']['isHidden'] = '0';      
               
				try {
					$Args['Node'] = new CRM_TaskCategory(array('id'=>$_POST['nodeID']));
                    $Args['Node']->addChild($Args['post']);
					$responseJson['success'] = 1;
					$responseJson['redirect'] = '/mcategories/task_categories/'.$Args['path'][3].'/index.html';
                } catch (SiteException $Exc) {
                    $responseJson['errors']['general'] = _('Ошибка добавления Типа задач');
                }
			}
		break;
        case 'edit':
            try {
                $Args['EObject'] = new CRM_TaskCategory(array('id' => $Args['path'][3]));
            } catch (SiteException $Exc) {
                header('Location: /mcategories/task_categories/'.$Args['path'][4].'/index.html');
                exit;
            }
			$Args['post'] = Misc::post($_POST['enter']);
			
			if (!isset($Args['post']['title']) || !strlen($Args['post']['title']))
                $responseJson['errors']['form']['title'] = _('Поле "Название" заполнено некорректно');
				
			if (count($responseJson['errors']) == 0) {
               
				try {
                    $Args['EObject']->edit($Args['post']);
					$responseJson['success'] = 1;
					$responseJson['redirect'] = '/mcategories/task_categories/'.$Args['path'][4].'/index.html';
                } catch (SiteException $Exc) {
                    $responseJson['errors']['general'] = _('Ошибка редактирования Типа задач');
                }
			}
		break;
        case 'delete':

            if (isset($_POST['del']) && strlen($_POST['del'])) {
				try {
					$Args['Node'] = new CRM_TaskCategory(array('id' => $Args['path'][2]));
					if ($Args['Node']->deleteChildren(array($_POST['del']))) {
						$responseJson['success'] = 1;
						$responseJson['redirect'] = '/mcategories/task_categories/'.$Args['path'][2].'/index.html';
					} else $responseJson['errors']['general'] = _('Ошибка удаления Типа задач');
				} catch (SiteException $Exc) {
					header('Location: /mcategories/task_categories/'.$Args['path'][2].'/index.html');
					exit;
				}
			}

		break;
	}
	print json_encode($responseJson);
    exit;
}

if (!isset($Args['path'][2])) $Args['path'][2] = '';

switch ($Args['path'][2]) {
	case 'add':
		$Args['Root'] = new CRM_TaskCategory(array('id'=>1));
		if (isset($Args['path'][3]) && $Args['path'][3] > 0) $Args['Node'] = new CRM_TaskCategory(array('id'=>$Args['path'][3]));
		else $Args['Node'] = $Args['Root'];
		$Args['Tree'] = $Args['Root']->getSubTree();
		$Args['Objects'] = $Args['Node']->getChildren();

		$tmpl = 'mcategories/task_categories/add.tpl';
	break;
	
	case 'edit':
        try {
            $Args['EObject'] = new CRM_TaskCategory(array('id' => $Args['path'][3]));
			$Args['Node'] = new CRM_TaskCategory(array('id' => $Args['EObject']->parent));
        } catch (SiteException $Exc) {
            header('Location: mcategories/task_categories/index.html');
            exit;
        }
		$Args['Root'] = new CRM_TaskCategory(array('id'=>1));
		$Args['Tree'] = $Args['Root']->getSubTree();

		$tmpl = 'mcategories/task_categories/edit.tpl';
	break;
	
	default:
		$Args['Root'] = new CRM_TaskCategory(array('id'=>1));
		if (isset($Args['path'][2]) && $Args['path'][2] > 0) $Args['Node'] = new CRM_TaskCategory(array('id'=>$Args['path'][2]));
		else $Args['Node'] = $Args['Root'];
		$Args['Tree'] = $Args['Root']->getSubTree();
		$Args['Objects'] = $Args['Node']->getChildren();

		$tmpl = 'mcategories/task_categories/index.tpl';
	break;
}
?>           