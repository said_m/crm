<?php   

if (isset($_POST) && count($_POST) > 0 && isset($_POST['action'])) {
    $responseJson['action'] = $_POST['action'];
    switch ($_POST['action'])
    {
		case 'get_users':
			$where = array();
			if (isset($_POST['q']) && strlen($_POST['q'])) {
				$where['append'] = "user_fname LIKE '%" .$_POST['q'] . "%' OR user_lname LIKE '%" .$_POST['q'] . "%'";
			}
			$Tmp = User::get(array('where'=>$where, 'order'=>'user_fname'));
			
			if (count($Tmp)) {
				$responseJson['success'] = 1;
				foreach ($Tmp as $Item) $responseJson['results'][] = array('id'=>(string)$Item->id, 'name'=>(string)($Item->user_lname . " " . $Item->user_fname));
			}
		break;
		case 'get_crm_categories':
			$where = array();
			if (isset($_POST['q']) && strlen($_POST['q'])) {
				$where['append'] = "title LIKE '%" .$_POST['q'] . "%'";
			}
			try {
				$Root = new CRM_TaskCategory(array('id'=>1));
				$Tmp = $Root->getSubTree(array('where'=>$where));
			} catch (SiteException $Exc) {}
			
			if (count($Tmp)) {
				$responseJson['success'] = 1;
				foreach ($Tmp as $Item) $responseJson['results'][] = array('id'=>(string)$Item->id, 'name'=>(string)($Item->title));
			}
		break;
		case 'get_calendar':
			if (!isset($_POST['date'])) $_POST['date'] = date('Y-m-d'); 
			$Args['TasksByHrs'] = CRM_Task::getTasksByHour(array('userID'=>$Args['User']->id, 'date'=>$_POST['date']));
			$Template = STemplate::getInstance();
			$Template->smarty->assign($Args);
			$responseJson['responseHTML'] = Misc::stripN($Template->smarty->fetch('mcrm/tasks/calendarlist.tpl'));
		break;

	}
   //Logger::getInstance()->debug($responseJson['responseHTML']);
	@print json_encode($responseJson);
	exit;
}
	
	$responseJson = array();
	switch ($Args['path'][1]) {
		case 'tree':
			try {
				$Root = new $Args['path'][2](array('id'=>1));
				$responseJson = getTreeChildren($Root);
			} catch (SiteException $Exc) {}
			
			print json_encode($responseJson);
			exit;
		break;
		case 'list':
			try {
				$Args['Node'] = new $Args['path'][3](array('id'=>$Args['path'][2]));
				$Args['Objects'] = $Args['Node']->getChildren();
			} catch (SiteException $Exc) {}
			
			if (get_class($Args['Node']) == 'CRM_Group') $list_tpl = 'mcategories/task_categories/list.tpl';
			elseif (get_class($Args['Node']) == 'CRM_Group') $list_tpl = 'mcategories/contact_groups/list.tpl';
			
			$Template = STemplate::getInstance();
			$Template->smarty->assign($Args);
			$responseJson['responseHTML'] = Misc::stripN($Template->smarty->fetch($list_tpl));
			
			print json_encode($responseJson);
			exit;
		break;
	}
	
	function getTreeChildren($Obj) {
		$res = array();
		$Tree = $Obj->getChildren();
		foreach ($Tree as $Item) {
			$tmp = array('label'=>$Item->title, 'id'=>$Item->id);
			if (($Item->rgt - $Item->lft) > 1) $tmp['children'] = getTreeChildren($Item);
			$res[] = $tmp;
		}
		return $res;
	}
	function rus2uni($str,$isTo = true)
    {
        $arr = array('ё'=>'&#x451;','Ё'=>'&#x401;');
        for($i=192;$i<256;$i++)
            $arr[chr($i)] = '&#x4'.dechex($i-176).';';
        $str =preg_replace(array('@([а-я]) @i','@ ([а-я])@i'),array('$1&#x0a0;','&#x0a0;$1'),$str);
        return strtr($str,$isTo?$arr:array_flip($arr));
    }
?>           