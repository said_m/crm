<?php   

$Args['CurModule'] = new Module(array('id'=>1));
$Args['perm'] = $Args['CurModule']->getPermission($Args['User']->id);
//---------------------------------------------------------------------------------------------

if (isset($_POST) && count($_POST) > 0 && isset($_POST['action'])) {
	$responseJson['action'] = $_POST['action'];
    switch ($_POST['action']) {
        case 'add':
			$Args['post'] = Misc::post($_POST['enter']);
            
			if (!isset($Args['post']['user_fname']) || !strlen($Args['post']['user_fname']))
                $responseJson['errors']['form']['user_fname'] = _('Поле "Имя" заполнено некорректно');
			
			if (!isset($Args['post']['user_lname']) || !strlen($Args['post']['user_lname']))
                $responseJson['errors']['form']['user_lname'] = _('Поле "Фамилия" заполнено некорректно');
				
			if (!isset($Args['post']['user_ogin']) || !filter_var($Args['post']['user_ogin'], FILTER_VALIDATE_EMAIL))
                $responseJson['errors']['form']['user_ogin'] = _('Поле "Логин/Email" заполнено некорректно');
            elseif (User::count(array('where' => array('user_ogin' => $Args['post']['user_ogin']))) > 0 || AuthUser::count(array('where' => array('login' => $Args['post']['user_ogin']))) > 0)
                $responseJson['errors']['form']['user_ogin'] = _('Пользователь с таким логином уже существует');
				
            if (!isset($Args['post']['user_word']) || strlen($Args['post']['user_word']) < 5)
                $responseJson['errors']['form']['user_word'] = _('Поле "Пароль" заполнено некорректно');
            elseif ($Args['post']['user_word'] != $_POST['ruser_word'])
                $responseJson['errors']['form']['ruser_word'] = _('Поля "Пароль" и "Повтор пароля" не совпадают');
			
			if (count($responseJson['errors']) == 0) {
				$Args['post']['user_word'] = md5($Args['post']['user_word']);
				$Args['post']['created'] = Misc::created();
                
				try {
                    $User = new User($Args['post'], 0, 1);
					if ($User) {
						$responseJson['success'] = 1;
						$responseJson['redirect'] = '/musers/index.html';
					} else $responseJson['errors']['general'] = _('Ошибка создания Пользователя');
                } catch (SiteException $Exc) {
                    $responseJson['errors']['general'] = _('Ошибка создания Пользователя');
                }
				try {
                    $post = array(
							'sys_userID'=>$User->id, 
							'login'=>$User->user_ogin, 
							'password'=>$User->user_word, 
							'sys_name'=>$Args['AuthUser']->sys_name
							);
					$AUser = new AuthUser($post, 0, 1);
					if (!$AUser) Logger::getInstance()->error('Ошибка создания Пользователя ('.print_r($post, true).')');
                } catch (SiteException $Exc) {
                    Logger::getInstance()->error('Ошибка создания Пользователя ('.print_r($post, true).')');
                }
			}
			
		break;
		
		case 'edit':
            try {
                $Args['EObject'] = new User(array('id' => $Args['path'][2]));
            } catch (SiteException $Exc) {
                header('Location: /musers/index.html');
                exit;
            }

			$Args['post'] = Misc::post($_POST['enter']);
            
			if ($Args['EObject']->user_ogin != $Args['post']['user_ogin'] 
				|| (strlen($Args['post']['user_word']) && $Args['EObject']->user_word != md5($Args['post']['user_word']))) $auth_changed = true;
			else $auth_changed = false;
			
			if (!isset($Args['post']['user_fname']) || !strlen($Args['post']['user_fname']))
                $responseJson['errors']['form']['user_fname'] = _('Поле "Имя" заполнено некорректно');
			
			if (!isset($Args['post']['user_lname']) || !strlen($Args['post']['user_lname']))
                $responseJson['errors']['form']['user_lname'] = _('Поле "Фамилия" заполнено некорректно');
				
			if (!isset($Args['post']['user_ogin']) || !filter_var($Args['post']['user_ogin'], FILTER_VALIDATE_EMAIL))
                $responseJson['errors']['form']['user_ogin'] = _('Поле "Логин/Email" заполнено некорректно');
            elseif (User::count(array('where' => array('user_ogin' => $Args['post']['user_ogin'], 'append' => 'id <> ' . $Args['EObject']->id))) > 0  || AuthUser::count(array('where' => array('login' => $Args['post']['user_ogin'],  'append' => 'sys_userID <> ' . $Args['EObject']->id))) > 0)
                $responseJson['errors']['form']['user_ogin'] = _('Пользователь с таким логином уже существует');
				
            if (isset($Args['post']['user_word']) && strlen($Args['post']['user_word'])) {
				if (strlen($Args['post']['user_word']) < 5)
					$responseJson['errors']['form']['user_word'] = _('Поле "Пароль" заполнено некорректно');
				elseif ($Args['post']['user_word'] != $_POST['ruser_word'])
					$responseJson['errors']['form']['ruser_word'] = _('Поля "Пароль" и "Повтор пароля" не совпадают');
			} else unset($Args['post']['user_word']);
            
			if (count($responseJson['errors']) == 0) {
				$post = array('login'=>$Args['post']['user_ogin']);
                if (isset($Args['post']['user_word'])) {$post['password'] = $Args['post']['user_word'] = md5($Args['post']['user_word']);}
				
				try {
					$AUser = new AuthUser(array('login'=>$Args['EObject']->user_ogin, 'password'=>$Args['EObject']->user_word));
					if ($AUser->login != $post['login'] || (isset($post['password']) && $AUser->password != $post['password']))
						$AUser->edit($post);
				} catch (SiteException $Exc) {
					Logger::getInstance()->error('Ошибка авторизации AuthUser ('.$Args['EObject']->user_ogin.':'.$Args['EObject']->user_word.')');
				}
				try {
					$Args['EObject']->edit($Args['post']);

					$responseJson['success'] = 1;
					$responseJson['redirect'] = '/musers/index.html';
				} catch (SiteException $Exc) {
					$responseJson['errors']['general'] = _('Ошибка редактирования Пользователя');
				}
			}
			
		break;
        case 'delete':

            if (isset($_POST['del']) && strlen($_POST['del'])) {
                if (User::delete($_POST['del'])) {
                    $responseJson['success'] = 1;
                    $responseJson['redirect'] = '/musers/index.html';
                }else
                    $responseJson['errors']['general'] = _('Ошибка удаления Пользователя');
            }


		break;
	}
	print json_encode($responseJson);
    exit;

}
//---------------------------------------------------------------------------------------------
if (!isset($Args['path'][1])) $Args['path'][1] = '';

switch ($Args['path'][1]) {
	case 'add':
		$tmpl = 'musers/add.tpl';
	break;
	case 'edit':
        try {
            $Args['EObject'] = new User(array('id' => $Args['path'][2]));
        } catch (SiteException $Exc) {
            header('Location: /musers/index.html');
            exit;
        }
		$tmpl = 'musers/edit.tpl';
	break;
	default:
        $Args['Sequence'] = new Paginator(User::count(), 30, $Args['path'], '');
        $Args['Objects'] = User::get(array('order' => 'user_lname', 'limit' => $Args['Sequence']->limit, 'offset' => $Args['Sequence']->skip));
		$tmpl = 'musers/index.tpl';
	break;
}
//---------------------------------------------------------------------------------------------

?>           