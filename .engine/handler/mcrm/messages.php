<?php

/**
 * @author Dmitriy Dunskiy
 * @copyright 2013
 */
if (is_array($_POST) && count($_POST) > 0) {
	$responseJson['action'] = $_POST['action'];
	switch ($_POST['action']) {	
		case 'add':
            
			$tousers = $Args['post'] = array();
            if (strlen($_POST['enter']['ccID']) > 0) $tousers = explode(',', substr($_POST['enter']['ccID'], 0, -1));
            unset($_POST['enter']['ccID']);
            $Args['post'] = Misc::post($_POST['enter']);

			if (!isset($Args['post']['message']) || !strlen($Args['post']['message']))
                $responseJson['errors']['form']['message'] = _('Поле "Сообщение" заполнено некорректно');

            if (!isset($Args['post']['touserID']) || $Args['post']['touserID'] <= 0) 
				$responseJson['errors']['form']['touserID'] = _('Не выбран адресат сообщения');
            
            if (count($responseJson['errors']) == 0) {
				$Args['post']['userID'] = $Args['User']->id;
				$Args['post']['created'] = date("Y-m-d H:i:s");
				$Args['post']['basic'] = 2;
				if (count($tousers) > 0) $Args['post']['cc_users'] = implode(',',array_values($tousers));

				try {
					$CRM_Message = new CRM_Message($Args['post'], 0, 1);

					$responseJson['success'] = 1;
					$responseJson['redirect'] = '/mcrm/messages/index.html';
					
					$Args['post']['basic'] = 1;
					if (count($tousers) > 0) {
						foreach ($tousers as $Args['post']['touserID']) {
							$CRM_Message = new CRM_Message($Args['post'], 0, 1);
						}
					}
                } catch (SiteException $Exc) {
                    $responseJson['errors']['general'] = _('Ошибка отправки сообщения');
                }
				
            }
		break;
     
	 	case 'delete':
            if (isset($_POST['del']) && strlen($_POST['del'])) {
				try {
					$Message = new CRM_Message(array('id' => $_POST['del']));
					if ($Message->edit(array('deleted'=>1))) {
						$responseJson['success'] = 1;
						$responseJson['redirect'] = '/mcrm/messages/index.html';
					} else $responseJson['errors']['general'] = _('Ошибка переноса в архив');
				} catch (SiteException $Exc) {
					header('Location: /mcrm/messages/index.html');
					exit;
				}
			}
     	break;
     	case 'kill':
            if (isset($_POST['del']) && strlen($_POST['del'])) {
                if (CRM_Message::delete($_POST['del'])) {
                    $responseJson['success'] = 1;
                    $responseJson['redirect'] = '/mcrm/messages/index.html';
                }else
                    $responseJson['errors']['general'] = _('Ошибка удаления сообщения');
            }
     	break;
	}   
	print json_encode($responseJson);
    exit;
}
//--------------------------------------------------------------------------------------------------------------------------
if (!isset($Args['path'][2])) $Args['path'][2] = '';

switch ($Args['path'][2]) {
	case 'deleted':
		$tmpl = 'mcrm/messages/index.tpl';                        
        $Args['allItems'] = CRM_Message::count(array('where'=>array('touserID'=>$Args['User']->id,'deleted'=>'0')));
		$Args['sentItems'] = CRM_Message::count(array('where'=>array('deleted'=>'0','basic'=>'2', 'userID'=>$Args['User']->id)));
		$Args['archiveItems'] = CRM_Message::count(array('where'=>array('deleted'=>'1', 'touserID'=>$Args['User']->id)));

		$Args['Sequence'] = new Paginator($Args['allItems'], 30, $Args['path'], '');
        $Args['Objects'] = CRM_Message::get(array('where'=>array('touserID'=>$Args['User']->id,'deleted'=>'1'), 'limit' => $Args['Sequence']->limit, 'offset' => $Args['Sequence']->skip));
		foreach ($Args['Objects'] as $CRMMessage) $CRMMessage->edit(array('viewed'=>'1'));	
	break;
	case 'sent':
		$tmpl = 'mcrm/messages/index.tpl';                        
        $Args['allItems'] = CRM_Message::count(array('where'=>array('touserID'=>$Args['User']->id,'deleted'=>'0')));
		$Args['sentItems'] = CRM_Message::count(array('where'=>array('deleted'=>'0','basic'=>'2', 'userID'=>$Args['User']->id)));
		$Args['archiveItems'] = CRM_Message::count(array('where'=>array('deleted'=>'1', 'touserID'=>$Args['User']->id)));
		
		$Args['Sequence'] = new Paginator($Args['allItems'], 30, $Args['path'], '');
        $Args['Objects'] = CRM_Message::get(array('where'=>array('userID'=>$Args['User']->id,'deleted'=>'0','basic'=>'2'), 'limit' => $Args['Sequence']->limit, 'offset' => $Args['Sequence']->skip));
	break;
	case 'add':
		$Args['allItems'] = CRM_Message::count(array('where'=>array('touserID'=>$Args['User']->id,'deleted'=>'0')));
		$Args['sentItems'] = CRM_Message::count(array('where'=>array('deleted'=>'0','basic'=>'2', 'userID'=>$Args['User']->id)));
		$Args['archiveItems'] = CRM_Message::count(array('where'=>array('deleted'=>'1', 'touserID'=>$Args['User']->id)));
                              
		$tmpl = 'mcrm/messages/add.tpl'; 
	break;
	default:
		$tmpl = 'mcrm/messages/index.tpl';                        
        $Args['allItems'] = CRM_Message::count(array('where'=>array('touserID'=>$Args['User']->id,'deleted'=>'0')));
		$Args['sentItems'] = CRM_Message::count(array('where'=>array('deleted'=>'0','basic'=>'2', 'userID'=>$Args['User']->id)));
		$Args['archiveItems'] = CRM_Message::count(array('where'=>array('deleted'=>'1', 'touserID'=>$Args['User']->id)));
		
		$Args['Sequence'] = new Paginator($Args['allItems'], 30, $Args['path'], '');
        $Args['Objects'] = CRM_Message::get(array('where'=>array('touserID'=>$Args['User']->id,'deleted'=>'0'), 'limit' => $Args['Sequence']->limit, 'offset' => $Args['Sequence']->skip));
		foreach ($Args['Objects'] as $CRMMessage) $CRMMessage->edit(array('viewed'=>'1'));				   
	break;
}

?>