<?php   

if (isset($_POST) && count($_POST) > 0 && isset($_POST['action'])) {
	$responseJson['action'] = $_POST['action'];
//Logger::getInstance()->debug(print_r($_POST, true));
    switch ($_POST['action']) {
        case 'add':
			$Args['post'] = Misc::post($_POST['enter']);
            
			if (!isset($Args['post']['title']) || !strlen($Args['post']['title']))
                $responseJson['errors']['form']['title'] = _('Поле "Заголовок" заполнено некорректно');
			
			if (count($responseJson['errors']) == 0) {
				$Args['post']['created'] = Misc::created();
				$Args['post']['createbyID'] = $Args['User']->id;
				if (!isset($Args['post']['userID']) || !strlen($Args['post']['userID'])) $Args['post']['userID'] = $Args['User']->id;
                
				try {
                    $Task = new CRM_Task($Args['post'], 0, 1);
					if ($Task) {
						$responseJson['success'] = 1;
						$responseJson['redirect'] = '/mcrm/tasks/index.html';
					} else $responseJson['errors']['general'] = _('Ошибка создания Задачи');
                } catch (SiteException $Exc) {
                    $responseJson['errors']['general'] = _('Ошибка создания Задачи');
                }
			}
			
		break;
		
		case 'edit':
            try {
                $Args['EObject'] = new CRM_Task(array('id' => $Args['path'][3]));
            } catch (SiteException $Exc) {
                header('Location: /mcrm/tasks/index.html');
                exit;
            }

			$Args['post'] = Misc::post($_POST['enter']);
            
			if (!isset($Args['post']['title']) || !strlen($Args['post']['title']))
                $responseJson['errors']['form']['title'] = _('Поле "Заголовок" заполнено некорректно');
            
			if (count($responseJson['errors']) == 0) {
				
				try {
					$Args['EObject']->edit($Args['post']);

					$responseJson['success'] = 1;
					$responseJson['redirect'] = '/mcrm/tasks/index.html';
				} catch (SiteException $Exc) {
					$responseJson['errors']['general'] = _('Ошибка редактирования Задачи');
				}
			}
			
		break;
        case 'delete':

            if (isset($_POST['del']) && strlen($_POST['del'])) {
                if (CRM_Task::delete($_POST['del'])) {
                    $responseJson['success'] = 1;
                    $responseJson['redirect'] = '/mcrm/tasks/index.html';
                }else
                    $responseJson['errors']['general'] = _('Ошибка удаления Задачи');
            }


		break;
	}
	print json_encode($responseJson);
    exit;

}
//---------------------------------------------------------------------------------------------
if (!isset($Args['path'][2])) $Args['path'][2] = '';

switch ($Args['path'][2]) {
	case 'add':
        try {
			$Root = new CRM_TaskCategory(array('id'=>1));
			$Args['TaskCategories'] = $Root->getSubTree();
        } catch (SiteException $Exc) {}
		$tmpl = 'mcrm/tasks/add.tpl';
	break;
	case 'edit':
        try {
			$Root = new CRM_TaskCategory(array('id'=>1));
			$Args['TaskCategories'] = $Root->getSubTree();
            $Args['EObject'] = new CRM_Task(array('id' => $Args['path'][3]));
        } catch (SiteException $Exc) {
            header('Location: /mcrm/tasks/index.html');
            exit;
        }
		$tmpl = 'mcrm/tasks/edit.tpl';
	break;
	case 'calendar':
		$Args['cdate'] = date('Y-m-d');
		$Args['TasksByHrs'] = CRM_Task::getTasksByHour(array('userID'=>$Args['User']->id, 'date'=>$Args['cdate']));
		$tmpl = 'mcrm/tasks/calendar.tpl';
	break;
	case 'others':
        $Args['Sequence'] = new Paginator(CRM_Task::count(array('where'=>array('createbyID'=>$Args['User']->id, 'append'=>'userID <> ' . $Args['User']->id))), 30, $Args['path'], '');
        $Args['Objects'] = CRM_Task::get(array('where'=>array('createbyID'=>$Args['User']->id, 'append'=>'userID <> ' . $Args['User']->id), 'order' => 'created', 'limit' => $Args['Sequence']->limit, 'offset' => $Args['Sequence']->skip));
		$tmpl = 'mcrm/tasks/index.tpl';
	break;
	default:
        $Args['Sequence'] = new Paginator(CRM_Task::count(array('where'=>array('userID'=>$Args['User']->id))), 30, $Args['path'], '');
        $Args['Objects'] = CRM_Task::get(array('where'=>array('userID'=>$Args['User']->id), 'order' => 'created', 'limit' => $Args['Sequence']->limit, 'offset' => $Args['Sequence']->skip));
		$tmpl = 'mcrm/tasks/index.tpl';
	break;
}
//---------------------------------------------------------------------------------------------

?>           