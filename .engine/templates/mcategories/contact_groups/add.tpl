{extends file='base.tpl'}
{block name=maincontent}
    <div id="content_wraper">
        <div id="content_container">

			{include file="includes/sub_menu.inc" m3='Группы контактов'}
			
			<div id="nav_thrd">
            	<a href="/mcategories/contact_groups/{$Node->id}/index.html"{if !isset($path[2]) || $path[2] == ''} class="active"{/if}>{t}Список{/t}</a>
                <a href="/mcategories/contact_groups/add/{$Node->id}/index.html"{if isset($path[2]) && $path[2] == 'add'} class="active"{/if}>{t}Добавить{/t}</a>
				{if $path[2] == 'edit'}<a href="/mcategories/contact_groups/edit/{$EObject->id}/{$Node->id}/index.html" class="active">{t}Редактировать{/t}</a>{/if}
            </div>
            
            <div style="clear:both;"></div>
				
			<form method="post" action="/mcategories/contact_groups/{$path[2]}/{if isset($EObject)}{$EObject->id}/{/if}{$Node->id}/" id="i_{$path[2]}_form"><input type="hidden" name="action" value="{$path[2]}"/>
				<div style="width:200px; float:left;">
					<div id="tree1" data-url="/ajax/tree/CRM_Group/"></div>
				</div>             
				<div style="width:270px; float:left; margin-left:40px;">
					<label for="i_title">{t}Название{/t}<br/><input id="i_title" name="enter[title]" type="text" value="{block name="i_title"}{/block}" /></label>
				</div>             
				<div style="width:270px; float:left; margin-left:40px;">
					<label for="i_sdesc">Описание</label><br/>
					<textarea name="enter[sdesc]" id="i_sdesc" rows="1" cols="1" style="width:270px; height:135px;">{block name="i_sdesc"}{/block}</textarea> 
					<div class="clears"></div>
				</div> 
				<div class="clears"></div>            
				<a class="action" href="javascript:void(0);">{block name="actionbutton"}{t}Добавить группу{/t}{/block}</a>
            	<input type="hidden" name="nodeID" value="{$Node->id}"/>
			</form>
        	
        	<div style="clear:both;"></div>
        </div>
    </div>
{/block}