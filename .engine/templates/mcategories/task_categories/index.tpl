{extends file='base.tpl'}
{block name=maincontent}
	<div id="dialog-confirm" title="{t}Удалить тип задач?{/t}">
		<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>{t}Этот тип задач будет удален из системы. Вы уверены?{/t}</p>
	</div>
	<form method="post" action="/mcategories/task_categories/{$Node->id}/" id="i_delete_form"><input type="hidden" name="action" value="delete"/><input id="i_del" type="hidden" name="del" value=""/></form>
    
	<div id="content_wraper">
        <div id="content_container">

			{include file="includes/sub_menu.inc" m3='Типы задач'}
			
			<div id="nav_thrd">
            	<a href="/mcategories/task_categories/{$Node->id}/index.html"{if !isset($path[3]) || $path[3] == ''} class="active"{/if}>{t}Список{/t}</a>
                <a href="/mcategories/task_categories/add/{$Node->id}/index.html"{if isset($path[3]) && $path[3] == 'add'} class="active"{/if}>{t}Добавить{/t}</a>
            </div>
            
            <div style="clear:both;"></div>
				
			<div style="width:200px; float:left;">
				<div id="tree1" data-url="/ajax/tree/CRM_TaskCategory/"></div>
			</div>             
			<div style="width:540px; float:left; margin-left:40px;">
				<table>
					<thead>
					<tr>
						<th>{t}Задача{/t}</th>
						<th>{t}Описание{/t}</th>
						<th>{t}Удалить{/t}</th>
					</tr>
					</thead>
					<tbody id="data_list">{include file="mcategories/task_categories/list.tpl"}</tbody>
				</table>            
			</div>             
        	
        	<div style="clear:both;"></div>
        </div>
    </div>
{/block}