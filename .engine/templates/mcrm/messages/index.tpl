{extends file='base.tpl'}
{block name=maincontent}
	{if $path[2] == 'deleted'}
	<div id="dialog-confirm" title="{t}Удалить сообщение?{/t}">
		<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>{t}Это сообщение будет удалено. Вы уверены?{/t}</p>
	</div>
	<form method="post" action="/mcrm/messages/" id="i_delete_form"><input type="hidden" name="action" value="kill"/><input id="i_del" type="hidden" name="del" value=""/></form>
	{else}
	<div id="dialog-confirm" title="{t}Переместить сообщение?{/t}">
		<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>{t}Это сообщение будет перенесено в архив. Вы уверены?{/t}</p>
	</div>
	<form method="post" action="/mcrm/messages/" id="i_delete_form"><input type="hidden" name="action" value="delete"/><input id="i_del" type="hidden" name="del" value=""/></form>
    {/if}
	
	<div id="content_wraper">
        <div id="content_container">

			{include file="includes/sub_menu.inc" m3='Сообщения'}
			
			<div id="nav_thrd">
            	<a href="/mcrm/messages/index.html"{if !isset($path[2]) || $path[2] == ''} class="active"{/if}>{t}Входящие{/t} ({$allItems})</a>
            	<a href="/mcrm/messages/sent/index.html"{if $path[2] == 'sent'} class="active"{/if}>{t}Отправленные{/t} ({$sentItems})</a>
            	<a href="/mcrm/messages/deleted/index.html"{if $path[2] == 'deleted'} class="active"{/if}>{t}Архив{/t} ({$archiveItems})</a>
                <a href="/mcrm/messages/add/index.html"{if $path[2] == 'add'} class="active"{/if}>{t}Добавить{/t}</a>
            </div>
            
            <div style="clear:both;"></div>
				<table>
					<thead>
					<tr>
						<th>{t}От кого{/t}</th>
						<th>{t}Дата{/t}</th>
						<th>{t}Сообщение{/t}</th>
						<th>{t}Удалить{/t}</th>
					</tr>
					</thead>
					<tbody id="data_list">{include file="mcrm/messages/list.tpl"}</tbody>
				</table>   
				<div id="d_pagination">{include file="includes/sequence.tpl"}</div>         
        	
        	<div style="clear:both;"></div>
        </div>
    </div>
{/block}