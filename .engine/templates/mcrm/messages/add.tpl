{extends file='musers/index.tpl'}

{block name=maincontent}
    <div id="content_wraper">
        <div id="content_container">

			{include file="includes/sub_menu.inc" m3='Сообщения'}
			
			<div id="nav_thrd">
            	<a href="/mcrm/messages/index.html"{if !isset($path[2]) || $path[2] == ''} class="active"{/if}>{t}Входящие{/t} ({$allItems})</a>
            	<a href="/mcrm/messages/sent/index.html"{if !isset($path[2]) || $path[2] == 'sent'} class="active"{/if}>{t}Отправленные{/t} ({$sentItems})</a>
            	<a href="/mcrm/messages/deleted/index.html"{if !isset($path[2]) || $path[2] == 'deleted'} class="active"{/if}>{t}Архив{/t} ({$archiveItems})</a>
                <a href="/mcrm/messages/add/index.html"{if isset($path[2]) && $path[2] == 'add'} class="active"{/if}>{t}Добавить{/t}</a>
            </div>
            
            <div style="clear:both;"></div>
            
			<form method="post" action="/mcrm/messages/{$path[2]}/" id="i_{$path[2]}_form"><input type="hidden" name="action" value="{$path[2]}"/>
				<div style="width:270px; float:left;">
					<div style="float:left; margin-right:20px;">
						<label for="i_touserID">{t}Кому{/t}</label><br/>
						<div id="ffb_user"></div>
						<input type="hidden" name="enter[touserID]" id="i_touserID" value="0" />
					</div>
					<div style="float:left; margin-right:20px;">
						<label for="i_cc_user_id">{t}Копия{/t}</label><br/>
						<div id="ffb_cc_user"></div>
						<input type="hidden" name="cc_user_id" id="i_cc_user_id" value="0" />
						<div id="сс_user_list"><table id="cc_list"></table></div>
						<input type="hidden" name="enter[ccID]" id="i_ccID" value="" />
					</div>
					
				</div>             
				<div style="width:270px; float:left; margin-left:40px;">
					<label for="i_sdesc">{t}Сообщение{/t}</label><br/>
					<textarea name="enter[message]" id="i_message" rows="1" cols="1" style="width:270px; height:135px;">{block name="i_message"}{/block}</textarea> 
					<div class="clears"></div>
				</div> 
				<div class="clears"></div>            
				<a class="action" href="javascript:void(0);">{block name="actionbutton"}{t}Отправить сообщение{/t}{/block}</a>
            </form>
			
        	<div style="clear:both;"></div>
        </div>
    </div>
{/block}