{extends file='mcrm/tasks/add.tpl'}

{block name="i_title"}{$EObject->title|escape:'html'}{/block}
{block name="i_priority"}
<option value="high"{if $EObject->priority == 'high'} selected="selected"{/if}>{t}Высокий{/t}</option>
<option value="medium"{if $EObject->priority == 'medium'} selected="selected"{/if}>{t}Средний{/t}</option>
<option value="low"{if $EObject->priority == 'low'} selected="selected"{/if}>{t}Низкий{/t}</option>
{/block}
{block name="i_expiry_date"}{$EObject->expiry_date}{/block}
{block name="i_expiry_time"}{$EObject->expiry_time}{/block}
{block name="i_about"}{$EObject->about|escape:'html'}{/block}
{block name="i_categoryID"}
{foreach $TaskCategories as $Category}<option value="{$Category->id}"{if $EObject->categoryID == $Category->id} selected="selected"{/if}>{'&nbsp;&nbsp;&nbsp;'|repeat:($Category->level-2)}{$Category->title}</option>{/foreach}
{/block}

{block name="i_state"}
<label for="i_state">{t}Статус{/t}<br/>
	<select id="i_state" name="enter[state]">
		<option value="open"{if $EObject->state == 'open'} selected="selected"{/if}>{t}Открыт{/t}</option>
		<option value="done"{if $EObject->state == 'done'} selected="selected"{/if}>{t}Выполнен{/t}</option>
		<option value="not done"{if $EObject->state == 'not done'} selected="selected"{/if}>{t}Не выполнен{/t}</option>
		<option value="attempted"{if $EObject->state == 'attempted'} selected="selected"{/if}>{t}Была попытка{/t}</option>
		<option value="received call"{if $EObject->state == 'received call'} selected="selected"{/if}>{t}Получено требование{/t}</option>
		<option value="left message"{if $EObject->state == 'left message'} selected="selected"{/if}>{t}Оставлено сообщение{/t}</option>
		<option value="held"{if $EObject->state == 'held'} selected="selected"{/if}>{t}Отслеживается{/t}</option>
		<option value="not held"{if $EObject->state == 'not held'} selected="selected"{/if}>{t}Не отслеживается{/t}</option>
		<option value="postponed"{if $EObject->state == 'postponed'} selected="selected"{/if}>{t}Отложен{/t}</option>
		<option value="deleted"{if $EObject->state == 'deleted'} selected="selected"{/if}>{t}Удален{/t}</option>
	</select></label>
<div class="clears"></div>

{/block}

{block name="actionbutton"}{t}Сохранить{/t}{/block}
