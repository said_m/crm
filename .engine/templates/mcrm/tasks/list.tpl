{foreach $Objects as $Item name="list"}
<tr>
	<td><a href="/mcrm/tasks/edit/{$Item->id}/index.html">{$Item->title}</a></td>
	<td>{$Item->created|date_format:"%Y-%m-%d %H:%M"}</td>
	<td>{$Item->expiry_date} {$Item->expiry_time|date_format:"%H:%M"}</td>
	<td>{$Item->priority_title}</td>
	<td>{$Item->TaskCategory->title}</td>
	<td>{$Item->state_title}</td>
	<td align="center">
		<a href="javascript:void(0);" class="delete_object" rel="del_{$Item->id}"><img src="/i/delete.png" alt="delete"/></a>
	</td>
</tr>
{/foreach}
