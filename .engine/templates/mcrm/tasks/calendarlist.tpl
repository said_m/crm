{foreach $TasksByHrs as $Hr=>$Tasks name="list"}
	{if count($Tasks) == 0}
		<tr {if $Hr > 7 && $Hr < 18}bgcolor=#FFFCB7{/if}><td colspan="7">{$Hr}:00</td></tr>
	{else}
		{foreach $Tasks as $Task}
			<tr {if $Hr > 7 && $Hr < 18}bgcolor=#FFFCB7{/if} style="font-size: 9px;">
				<td>{$Task->expiry_time|date_format:"%H:%M"}</td>
				<td>{$Task->title}</td>
				<td>{$Task->created|date_format:"%Y-%m-%d %H:%M"}</td>
				<td>{$Task->priority_title}</td>
				<td align=center>{$Task->TaskCategory->title}</td>
				<td>{$Task->state_title}</td>
				<td></td>
			</tr>	
		{/foreach}
	{/if}
{/foreach}
