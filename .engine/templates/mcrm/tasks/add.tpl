{extends file='musers/index.tpl'}

{block name=maincontent}
    <div id="content_wraper">
        <div id="content_container">

			{include file="includes/sub_menu.inc" m3='Задачи'}
			
			<div id="nav_thrd">
            	<a href="/mcrm/tasks/index.html"{if $path[2] == ''} class="active"{/if}>{t}Мои задачи{/t}</a>
				<a href="/mcrm/tasks/others/index.html"{if $path[2] == 'others'} class="active"{/if}>{t}Мной поставленные задачи{/t}</a>
                <a href="/mcrm/tasks/add/index.html"{if $path[2] == 'add'} class="active"{/if}>{t}Добавить{/t}</a>
				{if isset($path[2]) && $path[2] == 'edit'}<a href="/mcrm/tasks/edit/{if isset($EObject)}{$EObject->id}/{/if}index.html" class="active">{t}Редактировать{/t}</a>{/if}
            </div>
            
            <div style="clear:both;"></div>
            
			<form method="post" action="/mcrm/tasks/{$path[2]}/{if isset($EObject)}{$EObject->id}/{/if}" id="i_{$path[2]}_form"><input type="hidden" name="action" value="{$path[2]}"/>
				<div style="width:270px; float:left;">
					<label for="i_title">{t}Заголовок{/t}<br/><input id="i_title" name="enter[title]" type="text" value="{block name="i_title"}{/block}" /></label>
					<div class="clears"></div>
					<label for="i_priority">{t}Приоритет{/t}<br/>
						<select id="i_priority" name="enter[priority]">
							{block name="i_priority"}<option value="high">Высокий</option>
							<option value="medium">Средний</option>
							<option value="low">Низкий</option>{/block}
						</select></label>
					<div class="clears"></div>
					<label for="i_userID">{t}Отвественный{/t}<br/><div id="ffb_user"></div>
						<input type="hidden" name="enter[userID]" id="i_userID" value="{$User->id}" />
					</label>
				</div>             
				<div style="width:270px; float:left; margin-left:40px;">
					<label for="i_categoryID">{t}Тип{/t}<br/>
						<select id="i_categoryID" name="enter[categoryID]">
							{block name="i_categoryID"}
							{foreach $TaskCategories as $Category}<option value="{$Category->id}">{'&nbsp;&nbsp;&nbsp;'|repeat:($Category->level-2)}{$Category->title}</option>{/foreach}
							{/block}
						</select></label>
					<div class="clears"></div>
					<label for="i_expiry_date">{t}Дата/время{/t}<br/><input id="i_expiry_date" name="enter[expiry_date]" type="text" value="{block name="i_expiry_date"}{/block}" /><input id="i_expiry_time" name="enter[expiry_time]" type="text" value="{block name="i_expiry_time"}{/block}" /></label>
					{block name="i_state"}{/block}
				</div>             
				<div style="width:270px; float:left; margin-left:40px;">
					<label for="i_about">{t}Описание{/t}</label><br/>
					<textarea name="enter[about]" id="i_about" rows="1" cols="1" style="width:270px; height:135px;">{block name="i_about"}{/block}</textarea> 
					<div class="clears"></div>
				</div> 
				<div class="clears"></div>            
				<a class="action" href="javascript:void(0);">{block name="actionbutton"}{t}Добавить задачу{/t}{/block}</a>
            </form>
			
        	<div style="clear:both;"></div>
        </div>
    </div>
{/block}
            <div style="float:left">
                <label for="from" style="float:left; padding:5px;">From</label>
                <input type="text" name="st_date" style="width: 80px; padding:7px; margin:0 15px 0 0;" id="st_date" value="{$st_date}" />
            </div>
