{extends file='base.tpl'}
{block name=maincontent}
	<div id="dialog-confirm" title="{t}Удалить задачу?{/t}">
		<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>{t}Эта задача будет удалена из системы. Вы уверены?{/t}</p>
	</div>
	<form method="post" action="/mcrm/tasks/" id="i_delete_form"><input type="hidden" name="action" value="delete"/><input id="i_del" type="hidden" name="del" value=""/></form>
	
    <div id="content_wraper">
        <div id="content_container">

			{include file="includes/sub_menu.inc" m3='Задачи'}
			
			<div id="nav_thrd">
            	<a href="/mcrm/tasks/index.html"{if $path[2] == ''} class="active"{/if}>{t}Мои задачи{/t}</a>
                <a href="/mcrm/tasks/others/index.html"{if $path[2] == 'others'} class="active"{/if}>{t}Мной поставленные задачи{/t}</a>
                <a href="/mcrm/tasks/add/index.html"{if $path[2] == 'add'} class="active"{/if}>{t}Добавить{/t}</a>
            </div>
            
            <div style="clear:both;"></div>
            <table>
                <thead>
                <tr>
                    <th>{t}Заголовок{/t}</th>
                    <th>{t}Дата создания{/t}</th>
                    <th>{t}Дата окончания{/t}</th>
                    <th>{t}Приоритет{/t}</th>
					<th>{t}Тип{/t}</th>
					<th>{t}Статус{/t}</th>
					<th>{t}Действия{/t}</th>
                </tr>
				</thead>
                <tbody id="data_list">{include file="mcrm/tasks/list.tpl"}</tbody>
            </table>            
        	<div id="d_pagination">{include file="includes/sequence.tpl"}</div>
        	<div style="clear:both;"></div>
        </div>
    </div>
{/block}