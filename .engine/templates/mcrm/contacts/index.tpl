{extends file='base.tpl'}
{block name=maincontent}
    <div id="content_wraper">
        <div id="content_container">

			{include file="includes/sub_menu.inc"}
			
            <div id="nav_thrd">
            	<a href="javascript:void(0);">Список</a>
                <a href="javascript:void(0);" class="active">Добавить</a>
                <a href="javascript:void(0);">Удалить</a>
                <a href="javascript:void(0);">Отменить</a>
            </div>
            
            <div style="clear:both;"></div>
            <table>
                <tr>
                    <th>ФИО</th>
                    <th>Логин</th>
                    <th>Должность</th>
                    <th>Дата создания</th>
                </tr>
                <tr>
                    <td><a href="javascript:void(0);">Василий Иванов</a></td>
                    <td>Технологии РПМ</td>
                    <td>Поставщик</td>
                    <td>16.01.2011</td>
                </tr>
                <tr>
                    <td><a href="javascript:void(0);">Петр Глушаков</a></td>
                    <td>Гугл Беларусь</td>
                    <td>Клиент</td>
                    <td>31.12.2012</td>
                </tr>
                <tr>
                    <td><a href="javascript:void(0);">Маргарита Ортман</a></td>
                    <td>Автоматика Директ</td>
                    <td>Клиент</td>
                    <td>28.05.2012</td>
                </tr>
                <tr>
                    <td><a href="javascript:void(0);">Иван Рагозин</a></td>
                    <td>Космос Телеком</td>
                    <td>Поставщик</td>
                    <td>23.02.2013</td>
                </tr>
                <tr>
                    <td><a href="javascript:void(0);">Нина Петровна</a></td>
                    <td>Белпочта</td>
                    <td>Аудитор</td>
                    <td>06.06.2013</td>
                </tr>
            </table>            
        	
        	<div style="clear:both;"></div>
        </div>
    </div>
{/block}