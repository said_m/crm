{extends file='musers/index.tpl'}

{block name=maincontent}
    <div id="content_wraper">
        <div id="content_container">

			{include file="includes/sub_menu.inc"}
			
			<div id="nav_thrd">
            	<a href="/musers/index.html"{if $path[1] == ''} class="active"{/if}>{t}Список{/t}</a>
                <a href="/musers/add/index.html"{if $path[1] == 'add'} class="active"{/if}>{t}Добавить{/t}</a>
				{if $path[1] == 'edit'}<a href="/musers/edit/{if isset($EObject)}{$EObject->id}/{/if}index.html" class="active">{t}Редактировать{/t}</a>{/if}
            </div>
            
            <div style="clear:both;"></div>
            
			<form method="post" action="/musers/{$path[1]}/{if isset($EObject)}{$EObject->id}/{/if}" id="i_{$path[1]}_form"><input type="hidden" name="action" value="{$path[1]}"/>
				<div style="width:270px; float:left;">
					<label for="i_user_ogin">{t}Логин/Email{/t}<br/><input id="i_user_ogin" name="enter[user_ogin]" type="text" value="{block name="i_user_ogin"}{/block}" /></label>
					<div class="clears"></div>
					<label for="i_user_lname">{t}Фамилия{/t}<br/><input id="i_user_lname" name="enter[user_lname]" type="text" value="{block name="i_user_lname"}{/block}" /></label>
					<div class="clears"></div>
					<label for="i_country">{t}Страна{/t}<br/><input id="i_country" name="enter[country]" type="text" value="{block name="i_country"}{/block}" /></label>
					<div class="clears"></div>
					<label for="i_user_address1">{t}Адрес{/t}<br/><input id="i_user_address1" name="enter[user_address1]" type="text" value="{block name="i_user_address1"}{/block}" /></label>
				</div>             
				<div style="width:270px; float:left; margin-left:40px;">
					<label for="i_user_word">{t}Пароль{/t}<br/><input id="i_user_word" name="enter[user_word]" type="password" /></label>
					<div class="clears"></div>
					<label for="i_user_fname">{t}Имя{/t}<br/><input id="i_user_fname" name="enter[user_fname]" type="text" value="{block name="i_user_fname"}{/block}" /></label>
					<div class="clears"></div>
					<label for="i_user_state">{t}Область{/t}<br/><input id="i_user_state" name="enter[user_state]" type="text" value="{block name="i_user_state"}{/block}" /></label>
					<div class="clears"></div>
					<label for="i_user_phone1">{t}Телефон{/t}<br/><input id="i_user_phone1" name="enter[user_phone1]" type="text" value="{block name="i_user_phone1"}{/block}" /></label>
				</div>             
				<div style="width:270px; float:left; margin-left:40px;">
					<label for="i_ruser_word">{t}Повторите пароль{/t}<br/><input id="i_ruser_word" type="password" name="ruser_word" type="text" /></label>
					<div class="clears"></div>
					<label for="i_user_mname">{t}Отчество{/t}<br/><input id="i_user_mname" name="enter[user_mname]" type="text" value="{block name="i_user_mname"}{/block}" /></label>
					<div class="clears"></div>
					<label for="i_user_city">{t}Город{/t}<br/><input id="i_user_city" name="enter[user_city]" type="text" value="{block name="i_user_city"}{/block}" /></label>
					<div class="clears"></div>
					<label for="i_user_mobile">{t}Мобильный телефон{/t}<br/><input id="i_user_mobile" name="enter[user_mobile]" type="text" value="{block name="i_user_mobile"}{/block}" /></label>
					<div class="clears"></div>
					<br />
				</div> 
				<div class="clears"></div>            
				<a class="action" href="javascript:void(0);">{block name="actionbutton"}{t}Добавить пользователя{/t}{/block}</a>
            </form>
			
        	<div style="clear:both;"></div>
        </div>
    </div>
{/block}