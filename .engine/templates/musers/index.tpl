{extends file='base.tpl'}
{block name=maincontent}
	<div id="dialog-confirm" title="{t}Удалить пользователя?{/t}">
		<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>{t}Этот пользователь будет удален из системы. Вы уверены?{/t}</p>
	</div>
	<form method="post" action="/musers/" id="i_delete_form"><input type="hidden" name="action" value="delete"/><input id="i_del" type="hidden" name="del" value=""/></form>
	
    <div id="content_wraper">
        <div id="content_container">

			{include file="includes/sub_menu.inc"}
			
			<div id="nav_thrd">
            	<a href="/musers/index.html"{if $path.1 == ''} class="active"{/if}>{t}Список{/t}</a>
                <a href="/musers/add/index.html"{if $path.1 == 'add'} class="active"{/if}>{t}Добавить{/t}</a>
            </div>
            
            <div style="clear:both;"></div>
            <table>
                <thead>
                <tr>
                    <th>{t}ФИО{/t}</th>
                    <th>{t}Логин{/t}</th>
                    <th>{t}Должность{/t}</th>
                    <th>{t}Дата создания{/t}</th>
					<th>{t}Удалить{/t}</th>
                </tr>
				</thead>
                <tbody id="data_list">{include file="musers/list.tpl"}</tbody>
            </table>            
        	<div id="d_pagination">{include file="includes/sequence.tpl"}</div>
        	<div style="clear:both;"></div>
        </div>
    </div>
{/block}