{extends file='base.tpl'}
{block name=maincontent}
    <div id="content_wraper">
        <div id="content_container">
            <div style="width:500px; float:left;">
            <table>
                <tr>
                    <th></th>
                    <th>Просроченный</th>
                    <th>30 дней</th>
                    <th>60 дней</th>
                    <th>120 дней</th>
                </tr>
                <tr>
                    <td>Минимально</td>
                    <td>0</td>
                    <td>5<span>750</span></td>
                    <td>3<span>450</span></td>
                    <td>7<span>1050</span></td>
                </tr>
                <tr>
                    <td>Незначительно</td>
                    <td>0</td>
                    <td>0</td>
                    <td>0</td>
                    <td>1<span>150</span></td>
                </tr>
                <tr>
                    <td>Возможно</td>
                    <td>3<span>450</span></td>
                    <td>0</td>
                    <td>0</td>
                    <td>1<span>150</span></td>
                </tr>
                <tr>
                    <td>Может</td>
                    <td>0</td>
                    <td>4<span>600</span></td>
                    <td>0</td>
                    <td>0</td>
                </tr>
            </table>
            </div>
            
            <div style="width:500px; float:left; margin-left:20px;">
            <table>
                <tr>
                    <th></th>
                    <th>Просроченный</th>
                    <th>30 дней</th>
                    <th>60 дней</th>
                    <th>120 дней</th>
                </tr>
                <tr>
                    <td>Минимально</td>
                    <td>0</td>
                    <td>5</td>
                    <td>3</td>
                    <td>7</td>
                </tr>
                <tr>
                    <td>Незначительно</td>
                    <td>0</td>
                    <td>0</td>
                    <td>0</td>
                    <td>1</td>
                </tr>
                <tr>
                    <td>Возможно</td>
                    <td>3</td>
                    <td>0</td>
                    <td>0</td>
                    <td>2</td>
                </tr>
                <tr>
                    <td>Может</td>
                    <td>0</td>
                    <td>4</td>
                    <td>0</td>
                    <td>0</td>
                </tr>
            </table>
            </div>
            
            <div style="clear:both; padding-top:20px;"></div>
            <table>
                <tr>
                    <th></th>
                    <th>Просроченный</th>
                    <th>30 дней</th>
                    <th>60 дней</th>
                    <th>120 дней</th>
                    <th>Более</th>
                </tr>
                <tr>
                    <td>Минимально</td>
                    <td>0</td>
                    <td>5<span>750</span></td>
                    <td>3<span>450</span></td>
                    <td>7<span>1050</span></td>
                    <td>0</td>
                </tr>
                <tr>
                    <td>Незначительно</td>
                    <td>0</td>
                    <td>0</td>
                    <td>0</td>
                    <td>1<span>150</span></td>
                    <td>2<span>300</span></td>
                </tr>
                <tr>
                    <td>Возможно</td>
                    <td>3<span>450</span></td>
                    <td>0</td>
                    <td>0</td>
                    <td>1<span>150</span></td>
                    <td>2<span>300</span></td>
                </tr>
                <tr>
                    <td>Может</td>
                    <td>0</td>
                    <td>4<span>600</span></td>
                    <td>0</td>
                    <td>1<span>150</span></td>
                    <td>0</td>
                </tr>
                <tr>
                    <td>Вероятно</td>
                    <td>1<span>150</span></td>
                    <td>1<span>150</span></td>
                    <td>1<span>150</span></td>
                    <td>1<span>150</span></td>
                    <td>8<span>1200</span></td>
                </tr>
            </table>            
                    
        	<div style="clear:both;"></div>
        </div>
    </div>
{/block}