<!DOCTYPE HTML>
<html>
<head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <title>ERP проект</title>
    <link rel="stylesheet" type="text/css" href="/style/reset.css"/>
    <link rel="stylesheet" type="text/css" href="/style/style.css"/>
    <link rel="stylesheet" type="text/css" href="/style/select2.css"/>
	<link rel="stylesheet" type="text/css" href="/style/form.css"/>
	<link rel="stylesheet" type="text/css" href="/style/jquery-ui.css"/>
	<link rel="stylesheet" type="text/css" href="/style/jqtree.css"/>
	<link rel="stylesheet" type="text/css" href="/style/flexbox.css"/>
	<link rel="stylesheet" type="text/css" href="/style/timepicker.css"/>
	
	<script type="text/javascript" src="/js/jquery.js"></script>
	<script type="text/javascript" src="/js/jquery-ui.js"></script>
	<script type="text/javascript" src="/js/js.js"></script>
	<script type="text/javascript" src="/js/select2.js"></script>
    <script type="text/javascript" src="/js/jqtree.js"></script>
    <script type="text/javascript" src="/js/jquery.carouFredSel-6.0.4-packed.js" ></script>
	<script type="text/javascript" src="/js/jquery.flexbox.js"></script>
	<script type="text/javascript" src="/js/timepicker.js"></script>
	
	<script type="text/javascript" src="/js/jquery.form.js"></script>
	<script type="text/javascript" src="/js/main.js"></script>
	{assign var="jsDIR" value="/js/"}
	{section loop=$path name="pth" start=0}
		{if strlen($path[pth])}
			{assign var="jsDIR" value="`$jsDIR``$path[pth]`/"}
			{assign var="jsPATH" value="`$smarty.server.DOCUMENT_ROOT``$jsDIR`js.js"}
			{if is_file($jsPATH)}<script language="javascript" type="text/javascript" src="{$jsDIR}js.js"></script>{/if}
		{/if}
	{/section}
	
	{block name=header}{/block}
</head>

<body>
	<div id="i_loading_dialog">{t}Загрузка. Подождите...{/t}</div>
    
	<!-- notifications, current contact and lout -->
    <div id="notify_nav_wraper">
    	<div id="notify_nav">
        
        	<!-- current contact -->
            <div id="current_contact">
            	<p>Текущий контакт:</p>
                <select id="selected_contact">
                    <option value="">Гугл Беларусь</option>
                    <option value="">Беларусь Калий</option>
                    <option value="">Транзитный Банк</option>
                    <option value="">Белорусская железная дорога</option>
                </select>
            </div>
            
            <!-- notofications and logout -->
            <div id="notify_msg">
            	<a href="javascript:void(0);" id="msg_adrop_sh">{if ($User->newCRMMessagesCnt+$User->newCRMTasksCnt) > 0}<span>{$User->newCRMMessagesCnt+$User->newCRMTasksCnt}</span>{/if}</a>
                <a href="javascript:void(0);"></a>
            	<p id="msg_pdrop_sh">{$User->user_fname} {$User->user_lname}</p>
                <div id="msg_adrop">
                	<a href="/mcrm/messages/index.html">Сообщения<span>{$User->newCRMMessagesCnt}</span></a>
                    <a href="/mcrm/tasks/index.html">Задачи<span>{$User->newCRMTasksCnt}</span></a>
                </div>
                <div id="msg_pdrop">
                	<a href="javascript:void(0);" id="i_h_logout">Выйти из системы</a>
                    <a href="javascript:void(0);" id="i_b_logout">Сменить профиль</a>
                </div>
            </div>
        
        </div>
    </div>  
    
    {include file="includes/main_menu.inc"}
            
{block name=maincontent}{/block}            
            

</body></html>            