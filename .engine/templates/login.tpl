<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <title>ERP project</title>
    <link rel="stylesheet" type="text/css" href="/style/reset.css"/>
    <link rel="stylesheet" type="text/css" href="/style/style.css"/>
	<link rel="stylesheet" type="text/css" href="/style/form.css"/>
	<link rel="stylesheet" type="text/css" href="/style/jquery-ui.css"/>
	<script type="text/javascript" src="/js/jquery.js"></script>
	<script type="text/javascript" src="/js/jquery-ui.js"></script>
	<script type="text/javascript" src="/js/jquery.form.js"></script>
	
	<script type="text/javascript" src="/js/login.js"></script>
	<script type="text/javascript" src="/js/main.js"></script>
</head>

<body>
    <div id="i_loading_dialog">{t}Загрузка. Подождите...{/t}</div>
    
	<div id="login_form">
        <form action="/" method="post" id="i_login_form"><input type="hidden" name="action" value="login"/>
            <label for="i_ogin">{t}Логин{/t}</label><br/>
            <input id="i_ogin" name="enter[ogin]" type="text" />
            <div class="clear"></div>
            
            <label for="i_word">{t}Пароль{/t}</label><br/>
            <input id="i_word" name="enter[word]" type="password"/>
            <div class="clear"></div>
            <!--
            <label for="signed" class="checkbox" style="margin-top:9px; float:left;">
            	<input type="checkbox" id="signed" name="rem" value="1" checked="checked" />
                <span>Я не человек</span>
            </label><br/>
			<div class="clear"></div>
			-->
            <label for="i_code">{t}Код с картинки{/t}</label><br/>
			<input id="i_code" name="code" type="text"  style="width:100px;" />
			<img id="cimg" src="/img.php" alt="" style="float:right;" />
			<div style="clear:both; margin-bottom:22px;"></div>                              
            
			<a class="action" href="javascript:void(0);" style="float:right;">{t}Войти{/t}</a>
			
			<a id="s_recover_login" href="javascript:void(0);">{t}Не можете войти?{/t}</a>
        </form>
    </div>
	
	<div id="recover_login">
		<form action="/" method="post" id="i_remind_form" onSubmit="alert('Ваш пароль будет выслан на Ваш e-mail.');"><input type="hidden" name="action" value="remind"/>
			<div style="clear:both; margin-bottom:10px;"></div>
			<p>{t}Для восстановления пароля введите e-mail.{/t}</p>
			<div style="clear:both; margin-bottom:20px;"></div>
			<label for="i_email">{t}E-mail{/t}</label><br/>
			<input type="text"  id="i_email" name="enter[email]" class="login_input" />
			<div style="clear:both; margin-bottom:27px;"></div>

			<a class="action" href="javascript:void(0);" style="float:right;">{t}Отправить{/t}</a>
			<div style="clear:both; margin-bottom:16px;"></div>
		</form>
	</div>

</body>

</html>