<!-- secondary menu -->
{$menu=$CurModule->Menu}
{if count($menu) > 0}
	<div id="nav_sec_wraper">
		<ul id="nav_sec">
			{foreach from=$menu item='mitem'}
				<li><a href="{$mitem.url}">{$mitem.title}</a></li>
			{/foreach}
		</ul>
		<a href="javascript:void(0);" id="sec_title" class="sec_slide">{if isset($m3) && strlen($m3) > 0}{$m3}{else}{$menu.0.title}{/if}<span>&nbsp;</span></a>
		<a href="javascript:void(0);" id="sec_title_close" class="sec_slide">Свернуть<span>&nbsp;</span></a>
	</div>
{/if}
