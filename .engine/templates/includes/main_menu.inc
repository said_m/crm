	<!-- main menu -->
    <div id="main_menu_wraper">
        <div id="main_menu">
        	<!-- main menu -->
			{$prm=array()}{$prm['order']='sort'}{$prm['where']=array()}{$prm['where']['mtype']='user'}
			{$Modules=Module::get($prm)}
            <div class="ms" id="mm">
                <ul class="nav" id="main">
                    {foreach from=$Modules item='Module' name='list'}
						<li id="a{$Module->style_id}"{if isset($CurModule) && $CurModule->id == $Module->id} class="active"{/if}><a href="{$Module->uri}">{$Module->title}</a></li>
					{/foreach}
                </ul>
                <a class="prev" href="#"></a>
                <a class="next" href="#"></a>
            </div>
            
            <!-- admin menu -->
			{$prm=array()}{$prm['order']='sort'}{$prm['where']=array()}{$prm['where']['mtype']='admin'}
			{$Modules=Module::get($prm)}
            <div class="ms" id="am">
                <ul class="nav" id="admin">
                    {foreach from=$Modules item='Module' name='list'}
						<li id="a{$Module->style_id}"{if isset($CurModule) && $CurModule->id == $Module->id} class="active"{/if}><a href="{$Module->uri}">{$Module->title}</a></li>
					{/foreach}
                </ul>
                <a class="prev" href="#"></a>
                <a class="next" href="#"></a>
            </div>
            
            <!-- menu switch -->
            <div id="nav_swap">
            	<a href="#mm"{if isset($CurModule) && $CurModule->mtype == 'user'} class='active'{/if}>Основное меню</a>
                <a href="#am"{if isset($CurModule) && $CurModule->mtype == 'admin'} class='active'{/if}>Администрирование</a>
            </div>
        </div>
    </div>
	{if isset($CurModule) && $CurModule->mtype == 'admin'}
		<script type="text/javascript">
			$(".ms").hide();
			$("#nav_swap a:last").addClass("active").show();
			$(".ms:last").show().find('ul').carouFredSel({
				circular: false,
				infinite: false,
				prev: {
					button: function() {
					return $(this).parents().find(".prev");
					}
				},
				next: {
					button: function() {
					return $(this).parents().find(".next");
					}
				},
				auto: false,
			});
		</script>
	{else}
		<script type="text/javascript">
			$(".ms").hide();
			$("#nav_swap a:first").addClass("active").show();
			$(".ms:first").show().find('ul').carouFredSel({
				circular: false,
				infinite: false,
				prev: {
					button: function() {
					return $(this).parents().find(".prev");
					}
				},
				next: {
					button: function() {
					return $(this).parents().find(".next");
					}
				},
				auto: false,
			});
		</script>
	{/if}