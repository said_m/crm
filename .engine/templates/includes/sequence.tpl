{if isset($Sequence) && count($Sequence->pages) > 1}
<div id="pagination">
    {if isset($Sequence->prev)}<a href="{$Sequence->prev}">&lsaquo;</a>{/if}
    
    {foreach from=$Sequence->pages key="currIdx" item="currView"}
        <a {if $currIdx eq $Sequence->current}class="current"{/if} href="{$currView}">{$currIdx+1}</a>
    {/foreach}
    {if isset($Sequence->next)}<a href="{$Sequence->next}">&rsaquo;</a>{/if}
</div>
<div style="clear:both;"></div>
{/if}